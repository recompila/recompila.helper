﻿Imports MySql.Data.MySqlClient
Imports MySql.Data
Imports System.Data

Namespace ADO
    Namespace MySql
        ''' <summary>
        ''' Clase para trabajar con ADO sobre bases de datos MYSQL
        ''' </summary>        
        Public Class cEjecutorSQL
            Inherits ADO.cEjecutorSQLBase
            Implements ADO.IEjecutorSQL

#Region " CONSTRUCTORES"
            ''' <summary>
            ''' Crea una nueva instancia de la clase para poder trabajar con la base de datos que se le pasa como parametro
            ''' </summary>
            Public Sub New(ByVal eDatosConexion As ADO.cConfiguracionAcceso)
                MyBase.New(eDatosConexion)
            End Sub
#End Region

#Region " METODOS AUXILIARES COMPARTIDOS "
            Public Shared Function escaparCadena(ByVal eEntrada As String) As String
                If String.IsNullOrEmpty(eEntrada) Then Return ""

                '\0  An ASCII NUL (0x00) character.  
                '\'  A single quote ("'") character.  
                '\"  A double quote (""") character.  
                '\b  A backspace character.  
                '\n  A newline (linefeed) character.  
                '\r  A carriage return character.  
                '\t  A tab character.  
                '\Z  ASCII 26 (Control+Z). See note following the table.  
                '\\  A backslash ("\") character.  
                '\%  A "%" character. See note following the table.  
                '\_  A "_" character. See note following the table.  

                Return eEntrada.Replace("\", "\\").Replace(Chr(0), "\0").Replace("'", "\'").Replace("""", "\""")
            End Function
#End Region

#Region " IMPLEMENTACION DE ICOMANDOSQL "
#Region " CADENAS DE CONEXIÓN "
            Public Function obtenerCadenaConexion(ByVal eBBDD As String, _
                                                  Optional ByVal eServidor As String = "", _
                                                  Optional ByVal ePuerto As String = "",
                                                  Optional ByVal eUsuario As String = "", _
                                                  Optional ByVal eClave As String = "", _
                                                  Optional ByVal eTimeOut As Integer = 15) As String Implements IEjecutorSQL.obtenerCadenaConexion
                Dim CadenaDeConexion As String = ""

                If Not String.IsNullOrEmpty(eBBDD) Then CadenaDeConexion += "Database=" & eBBDD & ";"
                CadenaDeConexion += "Data Source=" & eServidor & ";"
                CadenaDeConexion += "Port=" & ePuerto & ";"
                CadenaDeConexion += "User Id=" & eUsuario & ";"
                CadenaDeConexion += "Password=" & eClave & ";"
                CadenaDeConexion += "Connection Timeout=" & eTimeOut & "; "

                Return CadenaDeConexion
            End Function

            Public Function obtenerCadenaConexion(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso) As String Implements IEjecutorSQL.obtenerCadenaConexion
                Return obtenerCadenaConexion(eConfiguracionConexion.BBDD, eConfiguracionConexion.Servidor, eConfiguracionConexion.Puerto, eConfiguracionConexion.Usuario, eConfiguracionConexion.Clave, eConfiguracionConexion.TimeOut)
            End Function

            Public Function obtenerCadenaConexion() As String Implements ADO.IEjecutorSQL.obtenerCadenaConexion
                Return obtenerCadenaConexion(DatosConexion)
            End Function
#End Region

#Region " METODOS DE CONEXIÓN "
            Function Conectar(Optional ByVal eGeneraExcepcion As Boolean = False) As System.Data.IDbConnection Implements ADO.IEjecutorSQL.Conectar
                Return Conectar(Me.DatosConexion, eGeneraExcepcion)
            End Function

            Public Function Conectar(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                                     Optional ByVal eGeneraExcepcion As Boolean = False) As System.Data.IDbConnection Implements IEjecutorSQL.Conectar
                Return Conectar(obtenerCadenaConexion(eConfiguracionConexion), eGeneraExcepcion)
            End Function

            Public Function Conectar(ByVal eCadenaConexion As String, _
                                     Optional ByVal eGeneraExcepcion As Boolean = False) As System.Data.IDbConnection Implements IEjecutorSQL.Conectar
                Dim paraDevolver As MySqlConnection = Nothing
                Dim elMensajeExepcion As String = String.Empty
                Dim laExcepcion As Exception = Nothing

                Dim laCadenaConexion As String = eCadenaConexion
                Try
                    paraDevolver = New MySqlConnection(laCadenaConexion)
                    paraDevolver.Open()
                Catch ex As System.Exception
                    Dim elMensajeError As String = "Error al conectar con la base de datos..."
                    If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeError, ex, New StackTrace(0, True))
                    laExcepcion = New Exception(elMensajeError, ex)

                    ' Si se llegó a abrir la conexión esta se cierra para evitar
                    ' que se consuman los recuross
                    If paraDevolver IsNot Nothing Then
                        paraDevolver.Close()
                        paraDevolver.Dispose()
                        paraDevolver = Nothing
                    End If
                End Try

                ' Si se configuró el lanzado de extepciones durante la conexión
                ' y se produjo durante la ejecución esta es lanzada antes de
                ' devolverel valor
                If eGeneraExcepcion AndAlso laExcepcion IsNot Nothing Then Throw laExcepcion

                Return paraDevolver
            End Function

            Function compruebaConexion(ByVal eTabla As String, _
                                       Optional ByVal eGeneraExcepcion As Boolean = False) As Boolean Implements IEjecutorSQL.compruebaConexion
                Return compruebaConexion(DatosConexion, eTabla, eGeneraExcepcion)
            End Function

            Function compruebaConexion(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                                       ByVal eTabla As String, _
                                       Optional ByVal eGeneraExcepcion As Boolean = False) As Boolean Implements IEjecutorSQL.compruebaConexion
                Dim paraDevolver As Boolean = False
                Dim laConexion As MySqlConnection = Nothing
                Dim elMensajeError As String = String.Empty
                Dim laExcepcion As Exception = Nothing

                Try
                    laConexion = Conectar(eConfiguracionConexion, False)
                    If laConexion IsNot Nothing Then
                        ' Se ejecuta una simple consulta para determinar cuantas filas
                        ' tiene la tabla que se le pasa como tabla de comprobación
                        Dim laConsulta As String = String.Empty
                        laConsulta = "SELECT COUNT(*) AS Total " & _
                                     "FROM " & eTabla & ";"

                        Using elComando As New MySqlCommand(laConsulta, laConexion)
                            elComando.ExecuteScalar()
                        End Using

                        ' Si el código llega hasta este punto la prueba se realizó correctamente
                        paraDevolver = True
                    Else
                        elMensajeError = "No se pudo obtener una conexión con la base de datos..."
                        If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeError, , New StackTrace(0, True))
                        laExcepcion = New Exception(elMensajeError, Nothing)
                        paraDevolver = False
                    End If
                Catch ex As MySqlException
                    elMensajeError = "Error al comprobar la conexión con la base de datos..."
                    If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeError, ex, New StackTrace(0, True))
                    laExcepcion = New Exception(elMensajeError, ex)
                    paraDevolver = False
                Finally
                    If laConexion IsNot Nothing Then
                        laConexion.Close()
                        laConexion.Dispose()
                        laConexion = Nothing
                    End If
                End Try

                ' Si se configuró el lanzado de extepciones durante la conexión
                ' y se produjo durante la ejecución esta es lanzada antes de
                ' devolverel valor
                If eGeneraExcepcion AndAlso laExcepcion IsNot Nothing Then Throw laExcepcion

                Return paraDevolver
            End Function
#End Region

#Region " METODOS DE EJECUCIÓN "
            Public Function ejecutarConsulta(ByVal eConsulta As String, _
                                             Optional ByVal eGeneraExcepcion As Boolean = False, _
                                             Optional ByVal eObtenerLastInsertId As Boolean = True) As Long Implements IEjecutorSQL.ejecutarConsulta
                Return ejecutarConsulta(DatosConexion, eConsulta, eGeneraExcepcion, eObtenerLastInsertId)
            End Function

            Public Function ejecutarConsulta(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                                             ByVal eConsulta As String, _
                                             Optional ByVal eGeneraExcepcion As Boolean = False, _
                                             Optional ByVal eObtenerLastInsertId As Boolean = True) As Long Implements IEjecutorSQL.ejecutarConsulta
                Dim paraDevolver As Long = -1
                Dim laConexion As MySqlConnection = Nothing
                Dim elMensajeError As String = String.Empty
                Dim laExcepcion As Exception = Nothing

                Try
                    laConexion = Conectar(eConfiguracionConexion)
                    If laConexion IsNot Nothing AndAlso laConexion.State = ConnectionState.Open Then
                        ' Se ejecuta la consulta sobre la conexión establecida
                        Using elComando As New MySqlCommand(eConsulta, laConexion)
                            paraDevolver = elComando.ExecuteNonQuery
                        End Using

                        ' Si la consulta fué de INSERT se obitene el último ID
                        ' insertado en la tabla afectada
                        If eConsulta.ToUpper.StartsWith("INSERT") AndAlso eObtenerLastInsertId Then
                            Dim elComandoID As New MySqlCommand("SELECT LAST_INSERT_ID();", laConexion)
                            paraDevolver = elComandoID.ExecuteScalar
                        End If
                    Else
                        elMensajeError = "No se pudo obtener una conexión con la base de datos..."
                        If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeError, , New StackTrace(0, True))
                        laExcepcion = New Exception(elMensajeError, Nothing)
                        paraDevolver = False
                    End If
                Catch ex As System.Exception
                    elMensajeError = "Error al ejecutar la consulta '" & eConsulta & "'..."
                    If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeError, ex, New StackTrace(0, True))
                    laExcepcion = New Exception(elMensajeError, ex)
                    paraDevolver = False
                Finally
                    If laConexion IsNot Nothing Then
                        laConexion.Close()
                        laConexion.Dispose()
                        laConexion = Nothing
                    End If
                End Try

                ' Si se configuró el lanzado de extepciones durante la conexión
                ' y se produjo durante la ejecución esta es lanzada antes de
                ' devolverel valor
                If eGeneraExcepcion AndAlso laExcepcion IsNot Nothing Then Throw laExcepcion

                Return paraDevolver
            End Function

            Function obtenerValor_1_1(ByVal eConsulta As String, _
                                      Optional ByVal eGeneraExcepcion As Boolean = False) As Long Implements IEjecutorSQL.obtenerValor_1_1
                Return obtenerValor_1_1(DatosConexion, eConsulta, eGeneraExcepcion)
            End Function

            Function obtenerValor_1_1(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                                      ByVal eConsulta As String, _
                                      Optional ByVal eGeneraExcepcion As Boolean = False) As Long Implements IEjecutorSQL.obtenerValor_1_1
                Dim paraDevolver As Long = -1
                Dim laConexion As MySqlConnection = Nothing
                Dim elMensajeError As String = String.Empty
                Dim laExcepcion As Exception = Nothing

                Try
                    laConexion = Conectar(eConfiguracionConexion)
                    If laConexion IsNot Nothing AndAlso laConexion.State = ConnectionState.Open Then
                        ' Se ejecuta la consulta sobre la conexión establecida
                        Using elComando = New MySqlCommand(eConsulta, laConexion)
                            paraDevolver = Funciones.NZL(elComando.ExecuteScalar & "")
                        End Using
                    Else
                        elMensajeError = "No se pudo obtener una conexión con la base de datos..."
                        If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeError, , New StackTrace(0, True))
                        laExcepcion = New Exception(elMensajeError, Nothing)
                        paraDevolver = -1
                    End If
                Catch ex As Exception
                    elMensajeError = "Error al ejecutar la consulta '" & eConsulta & "'..."
                    If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeError, ex, New StackTrace(0, True))
                    laExcepcion = New Exception(elMensajeError, ex)
                    paraDevolver = -1
                Finally
                    If laConexion IsNot Nothing Then
                        laConexion.Close()
                        laConexion.Dispose()
                        laConexion = Nothing
                    End If
                End Try

                ' Si se configuró el lanzado de extepciones durante la conexión
                ' y se produjo durante la ejecución esta es lanzada antes de
                ' devolverel valor
                If eGeneraExcepcion AndAlso laExcepcion IsNot Nothing Then Throw laExcepcion

                Return paraDevolver
            End Function

            Function obtenerValor_1_a(ByVal eConsulta As String, _
                                      ByVal eColumna As String, _
                                      Optional ByVal eGeneraExcepcion As Boolean = False) As String Implements IEjecutorSQL.obtenerValor_1_a
                Return obtenerValor_1_a(DatosConexion, eConsulta, eColumna, eGeneraExcepcion)
            End Function

            Function obtenerValor_1_a(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                                      ByVal eConsulta As String, _
                                      ByVal eColumna As String, _
                                      Optional ByVal eGeneraExcepcion As Boolean = False) As String Implements IEjecutorSQL.obtenerValor_1_a
                Dim paraDevolver As String = String.Empty
                Dim laConexion As MySqlConnection = Nothing
                Dim elMensajeError As String = String.Empty
                Dim laExcepcion As Exception = Nothing

                Dim elReader As MySqlDataReader = Nothing
                Try
                    elReader = obtenerReader(eConfiguracionConexion, eConsulta, eGeneraExcepcion)
                    If elReader IsNot Nothing Then
                        If elReader.Read Then
                            paraDevolver = elReader.Item(eColumna) & ""
                        End If
                    End If
                Catch ex As System.Exception
                    elMensajeError = "Error al obtener el reader de '" & eConsulta & "'..."
                    If Log._LOG_ACTIVO Then Log.escribirLog(eConsulta, ex, New StackTrace(0, True))
                    laExcepcion = New Exception(elMensajeError, ex)
                    paraDevolver = String.Empty
                Finally
                    If elReader IsNot Nothing Then elReader.Close()
                    elReader = Nothing
                End Try

                ' Si se configuró el lanzado de extepciones durante la conexión
                ' y se produjo durante la ejecución esta es lanzada antes de
                ' devolverel valor
                If eGeneraExcepcion AndAlso laExcepcion IsNot Nothing Then Throw laExcepcion

                Return paraDevolver
            End Function

            Public Function obtenerReader(ByVal eConsulta As String, _
                                          Optional ByVal eGeneraExcepcion As Boolean = False) As System.Data.IDataReader Implements IEjecutorSQL.obtenerReader
                Return obtenerReader(DatosConexion, eConsulta, eGeneraExcepcion)
            End Function

            Public Function obtenerReader(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                                          ByVal eConsulta As String, _
                                          Optional ByVal eGeneraExcepcion As Boolean = False) As System.Data.IDataReader Implements IEjecutorSQL.obtenerReader
                Dim laConexion As MySqlConnection = Nothing
                Dim elReader As MySqlDataReader = Nothing
                Dim elMensajeError As String = String.Empty
                Dim laExcepcion As Exception = Nothing

                Try
                    laConexion = Conectar(eConfiguracionConexion)
                    If laConexion IsNot Nothing AndAlso laConexion.State = ConnectionState.Open Then
                        Dim elComando As New MySqlCommand(eConsulta, laConexion)
                        elReader = elComando.ExecuteReader(CommandBehavior.CloseConnection)
                    Else
                        elMensajeError = "No se pudo obtener una conexión con la base de datos..."
                        If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeError, , New StackTrace(0, True))
                        laExcepcion = New Exception(elMensajeError, Nothing)
                        elReader = Nothing
                    End If
                Catch ex As Exception
                    elMensajeError = "Se ha producido un error al obtener el reader..."
                    If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeError, ex, New StackTrace(0, True))
                    laExcepcion = New Exception(elMensajeError, ex)
                    elReader = Nothing
                End Try

                ' Si se configuró el lanzado de extepciones durante la conexión
                ' y se produjo durante la ejecución esta es lanzada antes de
                ' devolverel valor
                If eGeneraExcepcion AndAlso laExcepcion IsNot Nothing Then Throw laExcepcion

                If elReader IsNot Nothing Then
                    If elReader.IsClosed Then
                        Try
                            If laConexion IsNot Nothing Then
                                laConexion.Close()
                                laConexion.Dispose()
                                laConexion = Nothing
                            End If
                        Catch ex As Exception
                        End Try
                        Return Nothing
                    Else
                        Return elReader
                    End If
                Else
                    Try
                        If laConexion IsNot Nothing Then
                            laConexion.Close()
                        End If
                    Catch ex As Exception
                    End Try
                    Return Nothing
                End If
            End Function
#End Region

#Region " METODOS DE AYUDA "
            Public Function yaExiste(ByVal eNombreTabla As String, _
                                     ByVal eNombreColumna As String, _
                                     ByVal eValorColumna As String, _
                                     Optional ByVal eCondicion As String = "") As Boolean Implements IEjecutorSQL.yaExiste
                Return yaExiste(DatosConexion, eNombreTabla, eNombreColumna, eValorColumna, eCondicion)
            End Function

            Public Function yaExiste(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                                     ByVal eNombreTabla As String, _
                                     ByVal eNombreColumna As String, _
                                     ByVal eValorColumna As String, _
                                     Optional ByVal eCondicion As String = "") As Boolean Implements IEjecutorSQL.yaExiste

                Dim laConsulta As String
                laConsulta = "SELECT COUNT(*) AS Total " & _
                             "FROM " & eNombreTabla & " " & _
                             "WHERE " & eNombreColumna & " = '" & eValorColumna & "' "

                If eCondicion <> "" Then anhadirCondicion(laConsulta, eCondicion)

                Return (obtenerValor_1_1(eConfiguracionConexion, laConsulta) > 0)
            End Function

            Public Function obtenerSiguienteID(ByVal enombreTabla As String, _
                                               ByVal eNombreColumna As String) As Integer Implements IEjecutorSQL.obtenerSiguienteID
                Return obtenerSiguienteID(DatosConexion, eNombreColumna, eNombreColumna)
            End Function

            Public Function obtenerSiguienteID(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                                               ByVal enombreTabla As String, _
                                               ByVal eNombreColumna As String) As Integer Implements IEjecutorSQL.obtenerSiguienteID
                Dim laConsulta As String
                laConsulta = "SELECT MAX(" & eNombreColumna & ") " & _
                             "FROM " & enombreTabla

                Return (Funciones.NZL(obtenerValor_1_1(eConfiguracionConexion, laConsulta)) + 1)
            End Function

            Public Function obtenerSiguienteID(ByVal enombreTabla As String) As Integer Implements IEjecutorSQL.obtenerSiguienteID
                Return obtenerSiguienteID(DatosConexion, enombreTabla)
            End Function

            Public Function obtenerSiguienteID(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                                               ByVal enombreTabla As String) As Integer Implements IEjecutorSQL.obtenerSiguienteID
                Dim laConsulta As String
                laConsulta = "SELECT MAX(Id) " & _
                             "FROM " & enombreTabla

                Return (Funciones.NZL(obtenerValor_1_1(eConfiguracionConexion, laConsulta)) + 1)
            End Function
#End Region

#Region " METODOS DDL "
            Public Function obtenerTablas() As System.Collections.Generic.List(Of String) Implements IEjecutorSQL.obtenerTablas
                Dim ParaDevolver As New List(Of String)

                Dim laConsulta As String
                laConsulta = "SHOW TABLES;"

                Dim elReader As MySqlDataReader = obtenerReader(laConsulta)
                If elReader IsNot Nothing Then
                    While elReader.Read
                        If Not ParaDevolver.Contains(elReader(0)) Then ParaDevolver.Add(elReader(0))
                    End While
                    elReader.Close()
                End If
                elReader = Nothing

                Return ParaDevolver
            End Function
#End Region
#End Region
        End Class
    End Namespace
End Namespace
