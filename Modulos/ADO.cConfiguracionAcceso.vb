﻿Namespace ADO
    ''' <summary>
    ''' Objeto con la información necesaria para conectarse a cualquier base de datos.
    ''' Dependiendo de esta, algunas de las propiedades no serán cencesarias,
    ''' el objeto IEjecutorSQL que se instancie utilizará las propiedades que necesite
    ''' para su creación o ejecución de los metodos
    ''' </summary>
    Public Class cConfiguracionAcceso
        ''' <summary>
        ''' Nombre de la base de datos a la que se va a conectar
        ''' </summary>
        Public Property BBDD As String

        ''' <summary>
        ''' Nombre o IP del servidor donde se encuentra la base de datos
        ''' </summary>
        Public Property Servidor As String

        ''' <summary>
        ''' Puerto donde se encuentra escuchando el SGDB
        ''' </summary>
        Public Property Puerto As String

        ''' <summary>
        ''' Nombre del usuario con acceso a la base de datos
        ''' </summary>
        Public Property Usuario As String

        ''' <summary>
        ''' Clave del usuario con acceso a la base de datos
        ''' </summary>
        Public Property Clave As String

        ''' <summary>
        ''' TimeOut de ejecución de consultas
        ''' </summary>
        Public Property TimeOut As String

        ''' <summary>
        ''' Nombre del servicio donde se está ejecutando el SGDB
        ''' </summary>
        Public Property Servicio As String

        ''' <summary>
        ''' Parámetero(s) extra que se necesitan para establecer la conexión
        ''' </summary>
        Public Property ParametrosExtra As Object = Nothing
    End Class
End Namespace