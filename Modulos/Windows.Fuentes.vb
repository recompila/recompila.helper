Imports System.Drawing

Namespace Windows
    Namespace Fuentes
        Public Module modWindowsFuentes
            Public Function obtenerFuentes(ByVal eTamanho As Integer) As List(Of Font)
                Dim paraDevolver As New List(Of Font)

                For Each fuente As FontFamily In FontFamily.Families
                    Dim f As Font = Nothing

                    'Creamos las fuentes en funcion de los estilos disponibles
                    If Not String.IsNullOrEmpty(fuente.Name) AndAlso (fuente.IsStyleAvailable(FontStyle.Regular)) Then
                        f = New Font(fuente.Name, eTamanho)
                        'ElseIf (fuente.IsStyleAvailable(FontStyle.Bold)) Then
                        '    f = New Font(fuente.Name, tamanhoDibujoFuente, FontStyle.Bold)
                        'ElseIf (fuente.IsStyleAvailable(FontStyle.Italic)) Then
                        '    f = New Font(fuente.Name, tamanhoDibujoFuente, FontStyle.Italic)
                        'ElseIf (fuente.IsStyleAvailable(FontStyle.Underline)) Then
                        '    f = New Font(fuente.Name, tamanhoDibujoFuente, FontStyle.Underline)
                    End If

                    If f IsNot Nothing AndAlso Not paraDevolver.Contains(f) Then paraDevolver.Add(f)
                Next

                Return paraDevolver
            End Function

            ''' <summary>
            ''' Obtiene los tama�os de fuentes predeterminados para cargar en un combo
            ''' </summary>
            ''' <returns>Listado con los tama�os predeterminados</returns>
            Public Function obtenerTamanhos() As List(Of Short)
                Dim paraDevolver As New List(Of Short)

                With paraDevolver
                    .Clear()
                    .Add("8")
                    .Add("9")
                    .Add("10")
                    .Add("11")
                    .Add("12")
                    .Add("14")
                    .Add("16")
                    .Add("18")
                    .Add("20")
                    .Add("22")
                    .Add("24")
                    .Add("26")
                    .Add("28")
                    .Add("36")
                    .Add("72")
                End With

                Return paraDevolver
            End Function
        End Module
    End Namespace
End Namespace