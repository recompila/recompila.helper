﻿Imports System.Globalization

Namespace Fechas
    Public Module modFechas
        Public Function RFC822(ByVal eFecha As DateTime) As String
            Dim curTimeZone As TimeZone = TimeZone.CurrentTimeZone
            Dim currentOffset As TimeSpan = curTimeZone.GetUtcOffset(DateTime.Now)
            Dim zonaHoraria As String = ""

            ' Se calcula, dependiendo de la franja horaria y el offset, la diferencia horaria
            If currentOffset.Hours >= 0 Then
                zonaHoraria += "+"
            Else
                zonaHoraria += "-"
            End If

            zonaHoraria += currentOffset.Hours.ToString("D2")
            zonaHoraria += ":"
            zonaHoraria += currentOffset.Minutes.ToString("D2")

            ' Se formatea la hora para seguir el formato establecido en RFC822
            ' http://www.ietf.org/rfc/rfc0822.txt
            Dim paraDevolver As String = Format(eFecha, "yyyy-MM-dd") & "T" & Format(eFecha, "HH:mm:ss") & zonaHoraria
            Return paraDevolver
        End Function

        Public Function horasASegundos(ByVal laHora As String) As Long
            If laHora = "" Then Return 0
            Dim lasPartes() As String = laHora.Split(":")
            If lasPartes.Length < 3 Then Return 0

            Return (Funciones.NZL(lasPartes(0)) * 60 * 60) + (Funciones.NZL(lasPartes(1)) * 60) + Funciones.NZL(lasPartes(2))
        End Function

        Public Function mesesAAnhos(ByVal eLosMeses As Nullable(Of Long)) As String
            Dim paraDevolver As String = Nothing

            If eLosMeses IsNot Nothing AndAlso eLosMeses > 0 Then
                Dim LosAnhos As Long = eLosMeses \ 12
                Dim LosMeses As Long = eLosMeses Mod 12

                paraDevolver = LosAnhos & " años y " & LosMeses & " meses"
            End If

            Return paraDevolver
        End Function

        Public Function segundosAHoras(ByVal losSegundos As Long) As String
            Dim paraDevolver As String = "00:00:00"

            If losSegundos > 0 Then
                Dim Horas As Long = 0
                Dim Minutos As Long = 0
                Dim Segundos As Long = 0

                Try
                    Minutos = losSegundos \ 60
                    Segundos = losSegundos Mod 60

                    If Minutos > 0 Then
                        Horas = Minutos \ 60
                        Minutos = Minutos Mod 60
                    End If

                    paraDevolver = Format(Horas, "00") & ":" & Format(Minutos, "00") & ":" & Format(Segundos, "00")
                Catch
                    paraDevolver = "00:00:00"
                End Try
            End If

            Return paraDevolver
        End Function

        Public Function milisegundosAHoras(ByVal losMiliSegundos As Long) As String
            Dim paraDevolver As String = "00:00:00.000"

            If losMiliSegundos > 0 Then
                Dim Horas As Long = 0
                Dim Minutos As Long = 0
                Dim Segundos As Long = 0
                Dim Milisengundos As Long = 0

                Try
                    Segundos = losMiliSegundos \ 1000
                    Milisengundos = losMiliSegundos Mod 1000

                    If Segundos > 0 Then
                        Minutos = Segundos \ 60
                        Segundos = Segundos Mod 60
                    End If

                    If Minutos > 0 Then
                        Horas = Minutos \ 60
                        Minutos = Minutos Mod 60
                    End If

                    paraDevolver = Format(Horas, "00") & ":" & Format(Minutos, "00") & ":" & Format(Segundos, "00") & "." & Format(Milisengundos, "000")
                Catch
                    paraDevolver = "00:00:00.000"
                End Try
            End If

            Return paraDevolver
        End Function

        Public Function nombreDia(ByVal numeroDia As Integer) As String
            If numeroDia = 1 Then
                Return "Lunes"
            ElseIf numeroDia = 2 Then
                Return "Martes"
            ElseIf numeroDia = 3 Then
                Return "Miércoles"
            ElseIf numeroDia = 4 Then
                Return "Jueves"
            ElseIf numeroDia = 5 Then
                Return "Viernes"
            ElseIf numeroDia = 6 Then
                Return "Sábado"
            ElseIf numeroDia = 7 Then
                Return "Domingo"
            Else
                Return ""
            End If
        End Function

        Public Function nombreMes(ByVal numeroMes As Integer) As String
            If numeroMes = 1 Then
                Return "Enero"
            ElseIf numeroMes = 2 Then
                Return "Febrero"
            ElseIf numeroMes = 3 Then
                Return "Marzo"
            ElseIf numeroMes = 4 Then
                Return "Abril"
            ElseIf numeroMes = 5 Then
                Return "Mayo"
            ElseIf numeroMes = 6 Then
                Return "Junio"
            ElseIf numeroMes = 7 Then
                Return "Julio"
            ElseIf numeroMes = 8 Then
                Return "Agosto"
            ElseIf numeroMes = 9 Then
                Return "Septiembre"
            ElseIf numeroMes = 10 Then
                Return "Octubre"
            ElseIf numeroMes = 11 Then
                Return "Noviembre"
            ElseIf numeroMes = 12 Then
                Return "Diciembre"
            Else
                Return ""
            End If
        End Function

        Public Function nombreMesCorto(ByVal numeroMes As Integer) As String
            If numeroMes = 1 Then
                Return "Ene"
            ElseIf numeroMes = 2 Then
                Return "Feb"
            ElseIf numeroMes = 3 Then
                Return "Mar"
            ElseIf numeroMes = 4 Then
                Return "Abr"
            ElseIf numeroMes = 5 Then
                Return "May"
            ElseIf numeroMes = 6 Then
                Return "Jun"
            ElseIf numeroMes = 7 Then
                Return "Jul"
            ElseIf numeroMes = 8 Then
                Return "Ago"
            ElseIf numeroMes = 9 Then
                Return "Sep"
            ElseIf numeroMes = 10 Then
                Return "Oct"
            ElseIf numeroMes = 11 Then
                Return "Nov"
            ElseIf numeroMes = 12 Then
                Return "Dic"
            Else
                Return ""
            End If
        End Function

        Public Function nombreDiaCorto(ByVal numeroDia As Integer) As String
            If numeroDia = 1 Then
                Return "Lun"
            ElseIf numeroDia = 2 Then
                Return "Mar"
            ElseIf numeroDia = 3 Then
                Return "Mie"
            ElseIf numeroDia = 4 Then
                Return "Jue"
            ElseIf numeroDia = 5 Then
                Return "Vie"
            ElseIf numeroDia = 6 Then
                Return "Sab"
            ElseIf numeroDia = 7 Then
                Return "Dom"
            Else
                Return ""
            End If
        End Function

        Public Function obtenerSemanaAnho(ByVal dtPassed As DateTime) As Integer
            Dim ciCurr As CultureInfo = CultureInfo.CurrentCulture
            Dim weekNum As Integer = ciCurr.Calendar.GetWeekOfYear(dtPassed, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday)
            Return (weekNum)
        End Function

        Public Function edadPersona(ByVal fechaNacimiento As Date) As Integer
            Dim e As Single, a As Integer, m As Integer
            e = DateDiff("m", fechaNacimiento, Now) / 12
            a = Int(e)
            e = (e - a) * 12
            m = Int(e)
            Return (a)
        End Function
    End Module
End Namespace
