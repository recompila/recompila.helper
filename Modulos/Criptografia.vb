﻿Imports System.IO
Imports System.Windows.Forms
Imports System.Security.Cryptography
Imports System.Text

Namespace Criptografia
    Public Module modCriptografia
#Region " METODOS AUXILIARES "
        ''' <summary>
        ''' Crea una clave válida para la encriptación a partir de la clave especificada
        ''' </summary>
        Private Function crearKey(ByVal eClave As String) As Byte()
            Dim losDatos() As Char = eClave.ToCharArray
            Dim laLongitud As Integer = losDatos.GetUpperBound(0)
            Dim elArrayEntrada(laLongitud) As Byte

            For i As Integer = 0 To losDatos.GetUpperBound(0)
                elArrayEntrada(i) = CByte(Asc(losDatos(i)))
            Next

            Dim elSHA512 As New System.Security.Cryptography.SHA512Managed
            Dim elHash As Byte() = elSHA512.ComputeHash(elArrayEntrada)
            Dim elKey(31) As Byte

            For i As Integer = 0 To 31
                elKey(i) = elHash(i)
            Next

            Return elKey
        End Function

        ''' <summary>
        ''' Crea una clave IV válida a través de la clave especificada
        ''' </summary>
        Private Function crearIV(ByVal strPassword As String) As Byte()
            Dim losDatos() As Char = strPassword.ToCharArray
            Dim laLongitud As Integer = losDatos.GetUpperBound(0)
            Dim elArrayEntrada(laLongitud) As Byte

            For i As Integer = 0 To losDatos.GetUpperBound(0)
                elArrayEntrada(i) = CByte(Asc(losDatos(i)))
            Next

            Dim elSHA512 As New System.Security.Cryptography.SHA512Managed
            Dim elHash As Byte() = elSHA512.ComputeHash(elArrayEntrada)
            Dim elIV(15) As Byte

            For i As Integer = 32 To 47
                elIV(i - 32) = elHash(i)
            Next

            Return elIV
        End Function
#End Region

#Region " MD5 Y SHA256 "
        ''' <summary>
        ''' Obtiene el MD5 correspondiente a la cadena que se le pasa
        ''' </summary>
        ''' <param name="eCadena">Cadena de la que se quiere obtener el MD5</param>
        ''' <returns>MD5 calculado para la cadena de entrada</returns>
        Public Function encriptarEnMD5(ByVal eCadena As String) As String
            ' Si no se le pasa una cadena no se puede convertir
            If String.IsNullOrEmpty(eCadena) Then Return ""

            Dim elProveedor As New Security.Cryptography.MD5CryptoServiceProvider
            Dim losBytes() As Byte = System.Text.Encoding.ASCII.GetBytes(eCadena)
            Dim elResultado As New System.Text.StringBuilder()

            ' Encriptración en md5
            losBytes = elProveedor.ComputeHash(losBytes)

            For Each unByte As Byte In losBytes
                elResultado.Append(unByte.ToString("x2"))
            Next

            Return elResultado.ToString()
        End Function

        ''' <summary>
        ''' Obtiene el SHA256 correspondiente a la cadena especificada
        ''' </summary>
        ''' <param name="eCadena">Cadena de la que se quiere obtener el SHA256</param>
        ''' <returns>SHA256 calculado para la cadena de entrada</returns>
        Public Function encriptarEnSHA256(ByVal eCadena As String) As String
            ' Si no se le pasa una cadena no se puede convertir
            If String.IsNullOrEmpty(eCadena) Then Return ""

            ' Encriptacion en SHA256
            Dim elCodificador As New UnicodeEncoding()
            Dim losBytes() As Byte = elCodificador.GetBytes(eCadena)
            Dim elEncriptador As New System.Security.Cryptography.SHA256Managed()
            Dim elHash() As Byte = elEncriptador.ComputeHash(losBytes)

            Return Convert.ToBase64String(elHash)
        End Function

        ''' <summary>
        ''' Calcula el MD5 para el fichero que se pasa como argumento
        ''' </summary>
        ''' <param name="eRutaFichero">Fichero para el cálculo del MD5</param>
        ''' <returns>MD5 del fichero que se le pasa como parámetro</returns>
        Public Function calcularMD5Fichero(ByVal eRutaFichero As String) As String
            If System.IO.File.Exists(eRutaFichero) Then
                Dim laCadenaMD5 As String = String.Empty
                Dim elStreamFichero As FileStream = Nothing
                Dim losBytesFichero As [Byte]()
                Dim elProveedor As New MD5CryptoServiceProvider
                Try
                    elStreamFichero = File.Open(eRutaFichero, FileMode.Open, FileAccess.Read)
                    losBytesFichero = elProveedor.ComputeHash(elStreamFichero)
                    elStreamFichero.Close()
                    laCadenaMD5 = BitConverter.ToString(losBytesFichero)
                    laCadenaMD5 = laCadenaMD5.Replace("-", "")
                Catch ex As System.Exception
                    If Log._LOG_ACTIVO Then Log.escribirLog("Se ha producido un error al calcular el MD5 del fichero.", ex, New StackTrace(0, True))
                    Return ""
                End Try

                Return laCadenaMD5
            Else
                If Log._LOG_ACTIVO Then Log.escribirLog("ERROR. No existe el fichero de entrada en '" & eRutaFichero & "' para calcular MD5.", , New StackTrace(0, True))
                Throw New IO.FileNotFoundException
            End If
        End Function
#End Region

#Region " ENCRIPTACIÓN "
        ''' <summary>
        ''' Encripta el fichero especificado como parámetro con la clave que se le pasa
        ''' </summary>
        ''' <param name="eRutaFichero">Ruta del archivo que queremos comprimir</param>
        ''' <param name="eRutaFicheroEncriptado">Ruta del archivo resultante comprimido</param>
        ''' <param name="eClave">Clave utilizada para la encripcación</param>
        ''' <param name="eSegundaClave">Segunda clave de encripcación</param>
        ''' <returns>True o False dependiendo del resultado de la encriptación</returns>
        Public Function encriptarFichero(ByVal eRutaFichero As String, _
                                         ByVal eRutaFicheroEncriptado As String, _
                                         ByVal eClave As String, _
                                         Optional ByVal eSegundaClave As String = "") As Boolean
            ' Variables para controlar las excepciones
            Dim elMensajeExepcion As String = String.Empty
            Dim laExcepcion As Exception = Nothing
            Dim paraDevolver As Boolean = True

            ' Variables necesarias para la encriptación
            Dim fsOriginal As System.IO.FileStream = Nothing
            Dim fsEncriptado As System.IO.FileStream = Nothing
            Dim fsEncriptador As CryptoStream = Nothing

            ' Si no se indicó una segunda clave de encriptación
            ' se utiliza la misma que la primera
            If String.IsNullOrEmpty(eSegundaClave) Then eSegundaClave = eClave

            Try
                ' Apertura de los streams de entrada y salida
                fsOriginal = New System.IO.FileStream(eRutaFichero, FileMode.Open, FileAccess.Read)
                fsEncriptado = New System.IO.FileStream(eRutaFicheroEncriptado, FileMode.OpenOrCreate, FileAccess.Write)
                fsEncriptado.SetLength(0)

                ' Variables necesarias para la encriptación
                Dim elBuffer(4096) As Byte
                Dim losBytesProcesados As Long = 0
                Dim elTamanhoFichero As Long = fsOriginal.Length
                Dim losBytesBloqueActual As Integer

                ' Se crean los objetos necesariospara realizar la encriptación
                Dim elEncriptador As New System.Security.Cryptography.RijndaelManaged
                fsEncriptador = New CryptoStream(fsEncriptado, elEncriptador.CreateEncryptor(crearKey(eClave), crearIV(eSegundaClave)), CryptoStreamMode.Write)

                ' Se lee del fichero de entrada mediante bloques hasta 
                ' que no quede nada más que procesar, y se guardar encriptado
                ' en el de salida
                While losBytesProcesados < elTamanhoFichero
                    losBytesBloqueActual = fsOriginal.Read(elBuffer, 0, 4096)
                    fsEncriptador.Write(elBuffer, 0, losBytesBloqueActual)
                    losBytesProcesados = losBytesProcesados + CLng(losBytesBloqueActual)
                End While

                paraDevolver = True
            Catch ex As System.Exception
                elMensajeExepcion = "ERROR. Se ha producido un error al encriptar el fichero '" & eRutaFichero & "' en '" & eRutaFicheroEncriptado & "'."
                If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeExepcion, ex, New StackTrace(0, True))
                laExcepcion = New Exception(elMensajeExepcion, ex)
                paraDevolver = False
            Finally
                If fsEncriptador IsNot Nothing Then fsEncriptador.Close()
                If fsOriginal IsNot Nothing Then fsOriginal.Close()
                If fsEncriptado IsNot Nothing Then fsEncriptado.Close()
            End Try

            ' Si se produjo una excepción durante elproceso esta se lanza
            ' para indiar que el proceso no se pudo completar correctamente
            If laExcepcion IsNot Nothing Then Throw laExcepcion

            Return paraDevolver
        End Function

        ''' <summary>
        ''' Desencripta el fichero especificado como parámetro con la clave dada
        ''' </summary>
        ''' <param name="eRutaFichero">Ruta del archivo que queremos desencriptar</param>
        ''' <param name="eRutaFicheroDesencriptado">Ruta del archivo resultante desencriptado</param>
        ''' <param name="eClave">Clave utilizada para desencriptación</param>
        ''' <param name="eSegundaClave">Segunda clave de encripcación</param>
        ''' <returns>True o False dependiendo del resultado de la desencreiptación</returns>
        Public Function desencriptarFichero(ByVal eRutaFichero As String, _
                                            ByVal eRutaFicheroDesencriptado As String, _
                                            ByVal eClave As String, _
                                            Optional ByVal eSegundaClave As String = "") As Boolean
            Dim fsEncriptado As System.IO.FileStream = Nothing
            Dim fsDesencriptado As System.IO.FileStream = Nothing
            Dim cryptostream As CryptoStream = Nothing

            If String.IsNullOrEmpty(eSegundaClave) Then eSegundaClave = eClave

            Try

                'Setup file streams to handle input and output.
                fsEncriptado = New System.IO.FileStream(eRutaFichero, FileMode.Open, FileAccess.Read)
                fsDesencriptado = New System.IO.FileStream(eRutaFicheroDesencriptado, FileMode.OpenOrCreate, FileAccess.Write)
                fsDesencriptado.SetLength(0) 'make sure fsOutput is empty

                'Declare variables for encrypt/decrypt process.
                Dim bytBuffer(4096) As Byte 'holds a block of bytes for processing
                Dim lngBytesProcessed As Long = 0 'running count of bytes processed
                Dim lngFileLength As Long = fsEncriptado.Length 'the input file's length
                Dim intBytesInCurrentBlock As Integer 'current bytes being processed
                'Declare your CryptoServiceProvider.
                Dim cspRijndael As New System.Security.Cryptography.RijndaelManaged

                cryptostream = New CryptoStream(fsDesencriptado, cspRijndael.CreateDecryptor(crearKey(eClave), crearIV(eSegundaClave)), CryptoStreamMode.Write)


                'Use While to loop until all of the file is processed.
                While lngBytesProcessed < lngFileLength
                    'Read file with the input filestream.
                    intBytesInCurrentBlock = fsEncriptado.Read(bytBuffer, 0, 4096)

                    'Write output file with the cryptostream.
                    cryptostream.Write(bytBuffer, 0, intBytesInCurrentBlock)

                    'Update lngBytesProcessed
                    lngBytesProcessed = lngBytesProcessed + CLng(intBytesInCurrentBlock)
                End While

                Return True

            Catch ex As System.Exception
                Throw ex
                Return False
            Finally
                If cryptostream IsNot Nothing Then cryptostream.Close()
                If fsEncriptado IsNot Nothing Then fsEncriptado.Close()
                If fsDesencriptado IsNot Nothing Then fsDesencriptado.Close()
            End Try

        End Function
#End Region

#Region " BASE 64 "
        ''' <summary>
        ''' Convierte el string a base 64 string
        ''' </summary>
        ''' <param name="eEntrada">Cadena de entrada</param>
        ''' <returns>Cadena convertida a base 64</returns>
        Public Function string2Base64(ByVal eEntrada As String) As String
            If Not String.IsNullOrEmpty(eEntrada) Then
                Try
                    Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(eEntrada)
                    Return (Convert.ToBase64String(byt))
                Catch ex As Exception
                    Return String.Empty
                End Try
            Else
                Return eEntrada
            End If
        End Function

        ''' <summary>
        ''' Convierte una cadena en base 64 a una cadena nomrmal
        ''' </summary>
        ''' <param name="eEntrada">Cadena en base 64</param>
        ''' <returns>Cadena nomrmal</returns>
        Public Function base642String(ByVal eEntrada As String) As String
            If Not String.IsNullOrEmpty(eEntrada) Then
                Try
                    Dim b As Byte() = Convert.FromBase64String(eEntrada)
                    Return (System.Text.Encoding.UTF8.GetString(b))
                Catch ex As Exception
                    Return String.Empty
                End Try
            Else
                Return eEntrada
            End If
        End Function
#End Region
    End Module
End Namespace