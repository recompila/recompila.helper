﻿Imports System.Windows.Forms
Imports System.Net

Namespace Redes
    Namespace Internet
        Namespace HTTP

#Region "Clases"
            ''' <summary>
            ''' Clase encargada de la descarga de ficheros mediante HTTP
            ''' </summary>
            ''' <remarks></remarks>
            Public Class DescargaFicheroHTTP
                ''' <summary>
                ''' Distintos metodos de descarga que se pueden utilizar
                ''' </summary>
                ''' <remarks></remarks>
                Public Enum MetodosDescarga
                    Sync = 1
                    Async = 2
                End Enum

                Public BarraProgreso As Object                        ' Barra de progreso para indicar el estado de la descarga
                Private WithEvents ClienteWeb As Net.WebClient        ' Objeto para la descarga de ficheros
                Public Descargando As Boolean                         ' Controla si se están descargando ficheros
                Public MetodoDescarga As MetodosDescarga              ' Metodo de descarga que se va a utilizar


                ''' <summary>
                ''' Crea una nueva instancia de la Clase para la descarga de ficheros
                ''' </summary>
                ''' <param name="MetodoDescarga"></param>
                ''' <remarks></remarks>
                Public Sub New(ByVal MetodoDescarga As MetodosDescarga, _
                               Optional ByRef BarraProgreso As Object = Nothing)
                    Me.MetodoDescarga = MetodoDescarga
                    If BarraProgreso IsNot Nothing Then Me.BarraProgreso = BarraProgreso
                End Sub

                ''' <summary>
                ''' Inicia la descarga de la URL que se le pasa al archivo temporal
                ''' </summary>
                ''' <param name="urlRemota"></param>
                ''' <param name="archivoLocal"></param>
                ''' <remarks></remarks>
                Public Sub IniciarDescarga(ByVal urlRemota As String, _
                                           ByVal archivoLocal As String)
                    Try
                        If BarraProgreso IsNot Nothing Then BarraProgreso.Maximum = 100
                        If ClienteWeb Is Nothing Then ClienteWeb = New Net.WebClient
                        Descargando = True

                        If MetodoDescarga = MetodosDescarga.Async Then
                            ClienteWeb.DownloadFileAsync(New Uri(urlRemota), archivoLocal)
                        ElseIf MetodoDescarga = MetodosDescarga.Sync Then
                            ClienteWeb.DownloadFile(New Uri(urlRemota), archivoLocal)
                        End If
                    Catch ex As Exception
                        Dim Mensaje As String = "Error al descargar el archivo " & urlRemota & " a " & archivoLocal
                        Throw New Exception(Mensaje, ex)
                    End Try
                End Sub

                ''' <summary>
                ''' Se produce cada vez que hay un cambio en el estado de la descarga
                ''' </summary>
                ''' <param name="sender"></param>
                ''' <param name="e"></param>
                ''' <remarks></remarks>
                Private Sub CambioEstadoDescarga(ByVal sender As Object, _
                                                 ByVal e As System.Net.DownloadProgressChangedEventArgs) _
                                                 Handles ClienteWeb.DownloadProgressChanged
                    If BarraProgreso IsNot Nothing Then WinForms.ProgressBar.FijarBarra(BarraProgreso, CInt(Math.Round(e.ProgressPercentage)))
                End Sub

                ''' <summary>
                ''' Se produce cuando la descarga ha finalizado
                ''' </summary>
                ''' <param name="sender"></param>
                ''' <param name="e"></param>
                ''' <remarks></remarks>
                Private Sub DescargaCompleta(ByVal sender As Object, _
                                             ByVal e As System.ComponentModel.AsyncCompletedEventArgs) _
                                             Handles ClienteWeb.DownloadFileCompleted
                    Try
                        If BarraProgreso IsNot Nothing Then BarraProgreso.Value = 0
                        If ClienteWeb IsNot Nothing Then ClienteWeb.Dispose()
                    Catch ex As Exception
                        Dim Mensaje As String = "Error al finalizar la descarga de un fichero."
                        Throw New Exception(Mensaje, ex)
                    Finally
                        Descargando = False
                    End Try
                End Sub

            End Class
#End Region
            Public Module modRedesInternetHTTP

                ''' <summary>
                ''' Descarga el fichero Remoto al directorio que se le especifica.
                ''' Utiliza el mismo nombre del fichero remoto si no se le pasa ninguno, en caso
                ''' contrario, utilizará el que se le pasa como parametro
                ''' </summary>
                ''' <param name="urlRemota"></param>
                ''' <param name="directorioDestino"></param>
                ''' <param name="nombreFichero"></param>
                ''' <returns></returns>
                ''' <remarks></remarks>
                Public Function DescargarFichero(ByVal urlRemota As String, _
                                                 ByVal directorioDestino As String, _
                                                 Optional ByVal nombreFichero As String = "") As Boolean

                    Try
                        Dim ArchivoTemporal As String = Ficheros.ObtenerDirectorioTemporal

                        Dim DescargaFichero As New DescargaFicheroHTTP(DescargaFicheroHTTP.MetodosDescarga.Async, Nothing)
                        DescargaFichero.IniciarDescarga(urlRemota, ArchivoTemporal)
                        While DescargaFichero.Descargando
                            Application.DoEvents()
                        End While

                        If System.IO.File.Exists(ArchivoTemporal) Then
                            If nombreFichero = "" Then nombreFichero = System.IO.Path.GetFileName(urlRemota)
                            System.IO.File.Move(ArchivoTemporal, System.IO.Path.Combine(directorioDestino, nombreFichero))
                        End If
                    Catch ex As Exception
                        Dim Mensaje As String = "Error al descargar " & urlRemota & " a " & directorioDestino & " - " & nombreFichero
                        Throw New Exception(Mensaje, ex)
                        Return False
                    End Try

                    Return True
                End Function
            End Module
        End Namespace
    End Namespace
End Namespace
