﻿Imports System.Windows.Forms
Imports System.Net

Namespace Redes
    Namespace Internet
        Namespace Utilidades
            Public Module modRedesInternetUtilidades
                Public _TIME_OUT As Integer = 4000

                Private UltimaComprobacion As Date = Nothing
                Private HabiaInternet As Boolean = False
                Private Comprobando As Boolean = False

                Public Function descargarFichero(ByVal remoteUrl As String, _
                                                 ByVal localFilePath As String) As Boolean
                    Try
                        Dim fileDownloader As Net.WebClient = New Net.WebClient
                        fileDownloader.DownloadFile(New Uri(remoteUrl), localFilePath)
                    Catch ex As Exception
                        Return False
                    End Try
                    Return True
                End Function

                Public Function subirFichero(ByVal eLocal As String, _
                                        ByVal eRemoto As String, _
                                        ByVal eUsuario As String, _
                                        ByVal ePassword As String) As Boolean

                    Try
                        Dim fileUploader As Net.WebClient = New Net.WebClient
                        Dim Credenciales As New NetworkCredential(eUsuario, ePassword)
                        fileUploader.Credentials = Credenciales
                        fileUploader.UploadFile(New Uri(eRemoto), eLocal)
                    Catch ex As Exception
                        Return False
                    End Try
                    Return True
                End Function

                Public Function HayInternet(Optional ByVal eConMensaje As Boolean = False) As Boolean
                    If Comprobando Then Return HabiaInternet

                    Comprobando = True
                    Dim SeComprueba As Boolean = False
                    If UltimaComprobacion = Nothing Then
                        SeComprueba = True
                    Else
                        SeComprueba = (Math.Abs(DateDiff(DateInterval.Second, UltimaComprobacion, Now)) > 60)
                    End If

                    If SeComprueba Then
                        HabiaInternet = ComprobarInternet(_TIME_OUT)
                        UltimaComprobacion = Now
                    End If

                    If eConMensaje And Not HabiaInternet Then
                        _KryptonForms.MostrarMensaje("Para realizar esta operación es necesario que esté conectado a Internet. Revise el estado de la conexión y vuelva a intentarlo.", _
                                                     "Sin conexión a Internet", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If

                    Comprobando = False

                    Return HabiaInternet
                End Function

                ''' <summary>
                ''' Realiza una comprobación de conectividad con internet
                ''' Se valoraron varias opciones:
                '''   1) Hacer ping (Puede estar deshabilitado)
                '''   2) Leer contenido de google (es más seguro hacerlo con google, más improbable este caido)
                '''   3) Hacer un servicio para este propósito (programación de más)    
                ''' </summary> 
                Public Function ComprobarInternet(Optional ByVal eTimeOut As Integer = 5000) As Boolean
                    Dim ConexionInternet As Boolean = False
                    Try
                        If My.Computer.Network.IsAvailable() Then
                            Dim Peticion As Net.HttpWebRequest
                            Dim Respuesta As Net.HttpWebResponse
                            Dim StreamRespuesta As IO.Stream = Nothing

                            Dim laUrl As String = "http://www.google.com"
                            Dim miUri As System.Uri = New System.Uri(laUrl)
                            Peticion = WebRequest.Create(miUri)
                            'Peticion.Timeout = eTimeOut

                            Respuesta = Peticion.GetResponse()
                            StreamRespuesta = Respuesta.GetResponseStream()
                            Dim elContenido As String = New IO.StreamReader(StreamRespuesta).ReadToEnd()
                            StreamRespuesta.Close()

                            Return (elContenido.Trim.Length > 0)
                        End If
                    Catch ex As System.Exception
                        ' Realizando comprobaciones de que sucede al no poder conectarse (desconectar cable en el momento d
                        ' la peticion), se comprobo que se produce un error de resolución DNS, lo mismo que ocurriría si no
                        ' se puede conectar a internet para cualquier otra comprobacion, así que se considera que no hay
                        ' internet
                        ConexionInternet = False
                    End Try

                    Return ConexionInternet
                End Function
            End Module
        End Namespace
    End Namespace
End Namespace
