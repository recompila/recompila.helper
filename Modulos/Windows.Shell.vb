﻿Imports System.Runtime.InteropServices
Imports System.Windows.Forms

Namespace Windows
    Namespace Shell
        Public Module modSistema
            <StructLayout(LayoutKind.Sequential)> _
            Structure LASTINPUTINFO
                <MarshalAs(UnmanagedType.U4)> _
                Public cbSize As Integer
                <MarshalAs(UnmanagedType.U4)> _
                Public dwTime As Integer
            End Structure

            Public Class ResultadoShell
                Public Property ExitCode As Integer = 0
                Public Property HoraInicio As DateTime = DateTime.Now
                Public Property HoraFin As DateTime = DateTime.Now
                Public Property EjecutadoCorrectamente As Boolean = False

                Public ReadOnly Property TiempoEjecucion As TimeSpan
                    Get
                        Return HoraFin.Subtract(HoraInicio)
                    End Get
                End Property

                Public ReadOnly Property MilisegundosEjecucion As Double
                    Get
                        Return TiempoEjecucion.TotalMilliseconds
                    End Get
                End Property

                Public ReadOnly Property SegundosEjecucion As Double
                    Get
                        Return TiempoEjecucion.TotalSeconds
                    End Get
                End Property

                Public Sub New()

                End Sub

                Public Sub New(ByVal ps As System.Diagnostics.Process)
                    If ps IsNot Nothing Then
                        Try
                            ExitCode = ps.ExitCode
                            HoraFin = ps.ExitTime
                        Catch ex As Exception
                        End Try
                    End If
                End Sub
            End Class

            Dim idletime As Integer
            Dim lastInputInf As New LASTINPUTINFO()

            <DllImport("user32.dll")>
            Private Function GetLastInputInfo(ByRef plii As LASTINPUTINFO) As Boolean
            End Function

            ''' <summary>
            ''' Obtiene los segundos transcurridos desde la última actividad del usuario
            ''' </summary>
            Public Function SegundosInactividad() As Integer
                idletime = 0
                lastInputInf.cbSize = Marshal.SizeOf(lastInputInf)
                lastInputInf.dwTime = 0

                If GetLastInputInfo(lastInputInf) Then
                    idletime = Environment.TickCount - lastInputInf.dwTime
                End If

                If idletime > 0 Then
                    Return idletime / 1000
                Else
                    Return 0
                End If
            End Function

            Public Function ShellandWait(ByVal eEjecutable As String, _
                                         ByVal eArgumentos As String, _
                                         ByVal eWorkingDirectory As String, _
                                         ByVal eOculto As Boolean, _
                                         Optional ByVal eComprobarExistencia As Boolean = True, _
                                         Optional ByVal ePB2 As Object = Nothing) As ResultadoShell
                Dim objProcess As System.Diagnostics.Process = Nothing
                Dim AntiguoDirectorio As String = My.Computer.FileSystem.CurrentDirectory
                Dim AntiguoProceso As Integer = 0

                Dim HoraComienzo As DateTime
                Dim EjecucionCorrecta As Boolean = False
                Dim Resultado As ResultadoShell = Nothing

                If ePB2 IsNot Nothing Then
                    AntiguoProceso = ePB2.Value
                    WinForms.ProgressBar.FijarBarra(ePB2, 1)
                End If

                Try
                    My.Computer.FileSystem.CurrentDirectory = eWorkingDirectory

                    ' Si no existe el ejecutalbe no se continua...
                    If eComprobarExistencia Then
                        If Not IO.File.Exists(eEjecutable) Then
                            My.Computer.FileSystem.CurrentDirectory = AntiguoDirectorio

                            If ePB2 IsNot Nothing Then
                                ePB2.Style = ProgressBarStyle.Continuous
                                WinForms.ProgressBar.FijarBarra(ePB2, AntiguoProceso)
                            End If

                            Return Nothing
                        End If
                    End If

                    If Log._LOG_ACTIVO Then Log.escribirLog("Ejecutando '" & eEjecutable & " " & eArgumentos & "'...", , New StackTrace(0, True))
                    objProcess = New System.Diagnostics.Process()
                    objProcess.StartInfo.FileName = eEjecutable
                    If eArgumentos <> "" Then objProcess.StartInfo.Arguments = eArgumentos
                    If eOculto Then
                        objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
                    Else
                        objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal
                    End If
                    HoraComienzo = DateTime.Now
                    objProcess.Start()

                    ' Esperar hasta que el proceso termine
                    Dim AnteriorRefresco As Date = Date.Now
                    Do
                        If Math.Abs(DateDiff(DateInterval.Second, AnteriorRefresco, Date.Now)) >= 1 Then
                            AnteriorRefresco = Date.Now
                            If ePB2 IsNot Nothing Then ePB2.Refresh()
                        End If
                    Loop While Not objProcess.HasExited

                    EjecucionCorrecta = True
                Catch
                    If ePB2 IsNot Nothing Then
                        ePB2.Style = ProgressBarStyle.Continuous
                        WinForms.ProgressBar.FijarBarra(ePB2, AntiguoProceso)
                    End If

                    EjecucionCorrecta = False
                Finally
                    My.Computer.FileSystem.CurrentDirectory = AntiguoDirectorio

                    Resultado = New ResultadoShell(objProcess)
                    Resultado.HoraInicio = HoraComienzo
                    Resultado.EjecutadoCorrectamente = EjecucionCorrecta
                    Resultado.ExitCode = objProcess.ExitCode

                    ' Liberar los recursos
                    If objProcess IsNot Nothing Then objProcess.Close()
                End Try

                If ePB2 IsNot Nothing Then
                    WinForms.ProgressBar.FijarBarra(ePB2, AntiguoProceso)
                End If

                If Resultado IsNot Nothing AndAlso Resultado.EjecutadoCorrectamente Then
                    If Log._LOG_ACTIVO Then Log.escribirLog("Ejecución completada con éxito...", , New StackTrace(0, True))
                Else
                    If Log._LOG_ACTIVO Then Log.escribirLog("ERROR en la ejecución!!!", , New StackTrace(0, True))
                End If

                Return Resultado
            End Function

        End Module
    End Namespace
End Namespace