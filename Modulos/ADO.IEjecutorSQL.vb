﻿Namespace ADO
    ''' <summary>
    ''' Interfaz para trabajar con las distintas bases de datos
    ''' independientemente del motor ADO utilizado
    ''' </summary>
    ''' <remarks></remarks>
    Public Interface IEjecutorSQL
#Region " CADENAS DE CONEXIÓN "
        ''' <summary>
        ''' Obtiene una cadena de conexión ADO a partir de los parametros de conexión a la base de datos
        ''' </summary>
        ''' <param name="eBBDD">Nombre de la base de datos con la que se va a conectar</param>
        ''' <param name="eServidor">Nombre o IP del servidor donde está la base de datos</param>
        ''' <param name="ePuerto">Puerto en el que está la base de datos escuchando</param>
        ''' <param name="eUsuario">Usuario para el acceso al servidor</param>
        ''' <param name="eClave">Clave para el acceso al servidor</param>
        ''' <param name="eTimeOut">TimeOut para la cancelación de las consultas</param>
        Function obtenerCadenaConexion(ByVal eBBDD As String, _
                                       Optional ByVal eServidor As String = "", _
                                       Optional ByVal ePuerto As String = "",
                                       Optional ByVal eUsuario As String = "", _
                                       Optional ByVal eClave As String = "", _
                                       Optional ByVal eTimeOut As Integer = 15) As String

        ''' <summary>
        ''' Obtiene una cadena de conexion a partir de la estrucutra con la configuración de la base de datos
        ''' </summary>
        ''' <param name="eConfiguracionConexion">Estructura con los datos de acceso a la base de datos</param>
        ''' <returns>Cadena de conexión válida para el acceso a la base de datos</returns>
        Function obtenerCadenaConexion(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso) As String

        ''' <summary>
        ''' Obtiene una cadena de conexion a partir de la estrucutra con la configuración de la base de datos
        ''' </summary>
        ''' <returns>Cadena de conexión válida para el acceso a la base de datos instanciada en el objeto</returns>
        Function obtenerCadenaConexion() As String
#End Region

#Region " METODOS DE CONEXIÓN "
        ''' <summary>
        ''' Conecta con la base de datos configurada y devuelve el objeto conexión creado
        ''' </summary>
        ''' <param name="eGeneraExcepcion">Si se produce un error lanza una excepción</param>
        ''' <returns>Objeto IDbConnection creado</returns>
        Function Conectar(Optional ByVal eGeneraExcepcion As Boolean = False) As System.Data.IDbConnection

        ''' <summary>
        ''' Conecta con la base de datos de la que se le pasan los parámetros y devuelve
        ''' el objeto IDbConnection creado
        ''' </summary>
        ''' <param name="eConfiguracionConexion">Datos de acceso a la base de datos</param>
        ''' <param name="eGeneraExcepcion">Si se produce un error lanza una excepción</param>
        ''' <returns>Objeto IDbConnection creado o Nothing en caso de error</returns>
        Function Conectar(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                          Optional ByVal eGeneraExcepcion As Boolean = False) As System.Data.IDbConnection

        ''' <summary>
        ''' Conecta con la base de datos con los parametros establecidos en la cadena de conexion
        ''' y devuelve el objeto IDbConnection creado
        ''' </summary>
        ''' <param name="eCadenaConexion">Cadena de conexión utilizada para la conexión con la base de datos</param>
        ''' <param name="eGeneraExcepcion">Si se produce un error lanza una excepción</param>
        ''' <returns>Objeto IDbConnection creado o Nothing en caso de error</returns>
        Function Conectar(ByVal eCadenaConexion As String, _
                          Optional ByVal eGeneraExcepcion As Boolean = False) As System.Data.IDbConnection

        ''' <summary>
        ''' Realiza una comprobación de conexión a la base de datos con los parámetros que se le pasan
        ''' </summary>
        ''' <param name="eConfiguracionConexion">Datos de conexión configurados</param>
        ''' <param name="eTabla">Tabla sobre la que se va a realizar un SELECT para determinar la correcta conexión</param>
        ''' <param name="eGeneraExcepcion">Si se produce un error lanza una excepción</param>
        ''' <returns>True | False dependiendo del éxito de la prueba</returns>
        Function compruebaConexion(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                                   ByVal eTabla As String, _
                                   Optional ByVal eGeneraExcepcion As Boolean = False) As Boolean

        ''' <summary>
        ''' Realiza una comprobación de conexión a la base de datos con los parámetros que se le pasan
        ''' sobre la base de datos instanciada
        ''' </summary>            
        ''' <param name="eTabla">Tabla sobre la que se va a realizar la prueba</param>
        ''' <param name="eGeneraExcepcion">Si se produce un error lanza una excepción</param>
        ''' <returns>True | False dependiendo del éxito de la prueba</returns>
        Function compruebaConexion(ByVal eTabla As String, _
                                   Optional ByVal eGeneraExcepcion As Boolean = False) As Boolean
#End Region

#Region " METODOS DE EJECUCIÓN "
        ''' <summary>
        ''' Ejecuta la consulta que se le pasa por parametro
        ''' </summary>
        ''' <param name="eConfiguracionConexion">Estructura con los datos de conexión a la base de datos donde se ejecutará la consulta</param>
        ''' <param name="eConsulta">Consulta a ejecutar</param>
        ''' <returns>-1 si se produce un error, o el número de filas afectadas en otro caso</returns>
        ''' <remarks></remarks>
        Function ejecutarConsulta(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                                  ByVal eConsulta As String, _
                                  Optional ByVal eGeneraExcepcion As Boolean = False, _
                                  Optional ByVal eObtenerLastInsertId As Boolean = True) As Long

        ''' <summary>
        ''' Ejecuta la consulta que se le pasa por parametro enl a base de dato sinstanciada
        ''' </summary>
        ''' <param name="eConsulta">Consulta a ejecutar</param>
        ''' <returns>-1 si se produce un error, o el número de filas afectadas en otro caso</returns>
        ''' <remarks></remarks>
        Function ejecutarConsulta(ByVal eConsulta As String, _
                                  Optional ByVal eGeneraExcepcion As Boolean = False, _
                                  Optional ByVal eObtenerLastInsertId As Boolean = True) As Long

        ''' <summary>
        ''' Devuelve el valor de la columna 1 de la fila 1 obtenido en la consulta que se ejecuta.
        ''' </summary>
        ''' <param name="eConfiguracionConexion">Estructura con los datos de conexión a la base de datos donde se ejecutará la consulta</param>
        ''' <param name="eConsulta">Consulta para obtener el valor numérico </param>
        ''' <returns>0 Si se obtiene un valor NULL, -1 si se produce un error, o el valor obtenido</returns>
        Function obtenerValor_1_1(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                                  ByVal eConsulta As String, _
                                  Optional ByVal eGeneraExcepcion As Boolean = False) As Long

        ''' <summary>
        ''' Devuelve el valor de la columna 1 de la fila 1 obtenido en la consulta que se ejecuta.
        ''' </summary>
        ''' <param name="eConsulta">Consulta para obtener el parametro 1_1 </param>
        ''' <returns>0 Si se obtiene un valor NULL, -1 si se produce un error, o el valor obtenido</returns>
        ''' <remarks></remarks>
        Function obtenerValor_1_1(ByVal eConsulta As String, _
                                  Optional ByVal eGeneraExcepcion As Boolean = False) As Long

        ''' <summary>
        ''' Devuelve el valor de la columna que se le pasa como parametro y devuelve el valor cadena
        ''' </summary>
        ''' <param name="eConfiguracionConexion">Estructura con los datos de conexión a la base de datos donde se ejecutará la consulta</param>
        ''' <param name="eConsulta">Consulta para obtener el valor cadena </param>
        ''' <param name="eColumna">Nombre de la columna de donde se estraerá el dato</param>
        ''' <returns>Valor obtenido de la ejecución de la consulta</returns>
        Function obtenerValor_1_a(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                                  ByVal eConsulta As String, _
                                  ByVal eColumna As String, _
                                  Optional ByVal eGeneraExcepcion As Boolean = False) As String

        ''' <summary>
        ''' Devuelve el valor de la columna que se le pasa como parametro y devuelve el valor cadena
        ''' </summary>
        ''' <param name="eConsulta">Consulta para obtener el valor cadena </param>
        ''' <param name="eColumna">Nombre de la columna de donde se estraerá el dato</param>
        ''' <returns>Valor obtenido de la ejecución de la consulta</returns>
        Function obtenerValor_1_a(ByVal eConsulta As String, _
                                  ByVal eColumna As String, _
                                  Optional ByVal eGeneraExcepcion As Boolean = False) As String

        ''' <summary>
        ''' Obtiene un objeto IDataReader a partir de la consulta que se le pasa por parametro
        ''' </summary>
        ''' <param name="eConfiguracionConexion">Estructura con los datos de conexión a la base de datos donde se ejecutará la consulta</param>
        ''' <param name="eConsulta">Consulta para obtener el objeto IDataReader </param>
        ''' <returns>Objeto IDataReader obtenido a partir de la consulta</returns>
        Function obtenerReader(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                               ByVal eConsulta As String, _
                               Optional ByVal eGeneraExcepcion As Boolean = False) As System.Data.IDataReader

        ''' <summary>
        ''' Obtiene un Reader a partir de la consulta que se le pasa por parametro
        ''' </summary>
        ''' <param name="eConsulta">Consulta para obtener el objeto IDataReader </param>
        ''' <returns>Objeto IDataReader obtenido a partir de la consulta</returns>
        Function obtenerReader(ByVal eConsulta As String, _
                               Optional ByVal eGeneraExcepcion As Boolean = False) As System.Data.IDataReader
#End Region

#Region " METODOS DE AYUDA "
        ''' <summary>
        ''' Comprueba si ya existe un valor en la tabla para evitar repetidos
        ''' </summary>
        ''' <param name="eConfiguracionConexion">Estructura con los datos de conexión a la base de datos donde se ejecutará la consulta</param>
        ''' <param name="eNombreTabla">Nombre de la tabla sobre la que se va a actuar</param>
        ''' <param name="eNombreColumna">Nombre de la columna donde comprobar los repetidos</param>
        ''' <param name="eValorColumna">Valor que se está comprobando para la repetición</param>
        ''' <returns>True | False dependiendo de si existe o no.</returns>
        Function yaExiste(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                          ByVal eNombreTabla As String, _
                          ByVal eNombreColumna As String, _
                          ByVal eValorColumna As String, _
                          Optional ByVal eCondicion As String = "") As Boolean

        ''' <summary>
        ''' Comprueba si ya existe un valor en la tabla para evitar repetidos
        ''' </summary>
        ''' <param name="eNombreTabla">Nombre de la tabla sobre la que se va a actuar</param>
        ''' <param name="eNombreColumna">Nombre de la columna donde comprobar los repetidos</param>
        ''' <param name="eValorColumna">Valor que se está comprobando para la repetición</param>
        ''' <returns>True | False dependiendo de si existe o no.</returns>
        Function yaExiste(ByVal eNombreTabla As String, _
                          ByVal eNombreColumna As String, _
                          ByVal eValorColumna As String, _
                          Optional ByVal eCondicion As String = "") As Boolean

        ''' <summary>
        ''' Obtiene el siguiente ID libre en la tabla. 
        ''' OJO, no es bloqueante, es mejor crear un procedimiento almacenado o usar autonumericos
        ''' </summary>
        ''' <param name="eConfiguracionConexion">Estructura con los datos de conexión a la base de datos donde se ejecutará la consulta</param>
        ''' <param name="eNombreTabla">Nombre de la tabla sobre la que se va a actuar</param>
        ''' <param name="eNombreColumna">Nombre de la columna donde obtener el siguiente ID</param>
        ''' <returns>Siguiente ID libre obtenido en la tabla</returns>
        Function obtenerSiguienteID(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                                    ByVal enombreTabla As String, _
                                    ByVal eNombreColumna As String) As Integer

        ''' <summary>
        ''' Obtiene el siguiente ID libre en la tabla. 
        ''' OJO, no es bloqueante, es mejor crear un procedimiento almacenado u usar autonumericos
        ''' </summary>
        ''' <param name="eNombreTabla">Nombre de la tabla sobre la que se va a actuar</param>
        ''' <param name="eNombreColumna">Nombre de la columna donde obtener el siguiente ID</param>
        ''' <returns>Siguiente ID libre obtenido en la tabla</returns>
        Function obtenerSiguienteID(ByVal enombreTabla As String, _
                                    ByVal eNombreColumna As String) As Integer

        ''' <summary>
        ''' Obtiene el siguiente ID libre en la tabla. 
        ''' OJO, no es bloqueante, es mejor crear un procedimiento almacenado u usar autonumericos
        ''' </summary>
        ''' <param name="eConfiguracionConexion">Estructura con los datos de conexión a la base de datos donde se ejecutará la consulta</param>
        ''' <param name="eNombreTabla">Nombre de la tabla sobre la que se va a actuar</param>
        ''' <returns>Siguiente ID libre obtenido en la tabla</returns>
        Function obtenerSiguienteID(ByVal eConfiguracionConexion As ADO.cConfiguracionAcceso, _
                                    ByVal enombreTabla As String) As Integer

        ''' <summary>
        ''' Obtiene el siguiente ID libre en la tabla. 
        ''' OJO, no es bloqueante, es mejor crear un procedimiento almacenado u usar autonumericos
        ''' </summary>
        ''' <param name="eNombreTabla">Nombre de la tabla sobre la que se va a actuar</param>
        ''' <returns>Siguiente ID libre obtenido en la tabla</returns>
        Function obtenerSiguienteID(ByVal enombreTabla As String) As Integer
#End Region

#Region " METODOS DDL "
        ''' <summary>
        ''' Obtiene un listado con todas las tablas que componen la base de datos
        ''' </summary>
        ''' <returns>Listado con las tablas que componen la base de datos</returns>
        Function obtenerTablas() As List(Of String)
#End Region
    End Interface
End Namespace
