﻿Imports System.IO

Namespace Windows
    Namespace MenuInicio
        Public Module modMenuInicio
            Public rutaMenuInicio As String = Ficheros.CarpetasEspeciales.ObtenerRutaCarpetaEspecial(Ficheros.CarpetasEspeciales.mceIDLPaths.CSIDL_COMMON_PROGRAMS)
            Public rutaCarpetaStartUP As String = Ficheros.CarpetasEspeciales.ObtenerRutaCarpetaEspecial(Ficheros.CarpetasEspeciales.mceIDLPaths.CSIDL_COMMON_STARTUP)
            Public rutaCarpetaDesktop As String = Ficheros.CarpetasEspeciales.ObtenerRutaCarpetaEspecial(Ficheros.CarpetasEspeciales.mceIDLPaths.CSIDL_COMMON_DESKTOPDIRECTORY)

            Public Sub crearAccesoDirecto(ByVal eRutaAccesoDirecto As String, _
                                          ByVal eNombreAccesoDirecto As String, _
                                          ByVal eRutaEjecutable As String, _
                                          Optional ByVal eArgumentos As String = "")

                Dim laRuta As String = eRutaAccesoDirecto & "\" & eNombreAccesoDirecto & ".lnk"

                If Not File.Exists(laRuta) Then
                    Try
                        Dim ob As Object = CreateObject("WScript.Shell")
                        Dim vlnk As Object = ob.CreateShortcut(laRuta)
                        vlnk.Targetpath = eRutaEjecutable
                        If Not String.IsNullOrEmpty(eArgumentos) Then
                            vlnk.Arguments = eArgumentos
                        End If
                        vlnk.Save()
                    Catch ex As Exception
                        If Log._LOG_ACTIVO Then Log.escribirLog("Error al crear el acceso directo '" & laRuta & "'", ex, New StackTrace(0, True))
                    End Try
                End If
            End Sub

            Public Sub quitarAccesoDirecto(ByVal eRutaAccesoDirecto As String, _
                                           ByVal eNombreAccesoDirecto As String)

                Dim laRuta As String = eRutaAccesoDirecto & "\" & eNombreAccesoDirecto & ".lnk"
                If IO.File.Exists(laRuta) Then
                    Try
                        IO.File.Delete(laRuta)
                    Catch ex As Exception
                        If Log._LOG_ACTIVO Then Log.escribirLog("Error al eliminar el acceso directo '" & laRuta & "'", ex, New StackTrace(0, True))
                    End Try
                End If
            End Sub

        End Module
    End Namespace
End Namespace