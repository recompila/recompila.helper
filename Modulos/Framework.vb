﻿Imports System
Imports System.Runtime.InteropServices
Imports Microsoft.Win32
Imports System.Security
Imports System.ComponentModel

Namespace Framework
    Public Class cNetFramework
        Public Enum Version
            NF1 = 1
            NF2 = 2
            NF3 = 3
            NF3a = 4
            NF3b = 5
            NF3c = 6
            NF4 = 7
            NF4_b = 8
        End Enum

        Public _Is64Bit As Boolean
        Public _Is32Bit As Boolean

        Public Shared ReadOnly dotNETPath As String
        Public Shared ReadOnly dotNETVersion As String

        Public Shared Function checknetframework2() As String
            Return Environment.Version.ToString().Substring(0, 3)
        End Function

        Public Shared Function checknetframework() As String
            Dim NFv1 As String = Environment.Version.Major.ToString()
            Dim NFv2 As String = Environment.Version.Minor.ToString()
            Dim NFv3 As String = "v" & NFv1 & "." & NFv2 _
                    & "." & Environment.Version.Build.ToString()
            Dim RegKey As RegistryKey
            Dim InfoTemp As String

            Try
                If NFv1 = "2" And NFv2 = "0" Then
                    RegKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\NET Framework Setup\NDP\" _
                            & NFv3, False)
                    InfoTemp = RegKey.GetValue("SP").ToString()
                    If InfoTemp <> "0" Then
                        Return InfoTemp
                    Else
                        Return ""
                    End If
                Else
                    Return String.Empty
                End If
            Catch ex As Win32Exception
                Return ""
            End Try

        End Function

        Public Shared Function NFCheck(ByVal eVersion As Version) As Boolean
            ' Informacion sobre esto:
            ' http://stackoverflow.com/questions/199080/how-to-detect-what-net-framework-versions-and-service-packs-are-installed

            Const NF1 As String = "Software\Microsoft\NET Framework Setup\NDP\v1.1.4322"
            If Log._LOG_ACTIVO Then Log.escribirLog("NF1: '" & NF1 & "'", , New StackTrace(0, True))

            Const NF2 As String = "Software\Microsoft\NET Framework Setup\NDP\v2.0.50727"
            If Log._LOG_ACTIVO Then Log.escribirLog("NF2: '" & NF2 & "'", , New StackTrace(0, True))

            Const NF3 As String = "Software\Microsoft\NET Framework Setup\NDP\v3.0\Setup"
            If Log._LOG_ACTIVO Then Log.escribirLog("NF3: '" & NF3 & "'", , New StackTrace(0, True))
            Const NF3a As String = "Software\Microsoft\NET Framework Setup\NDP\v3.0\Setup\Windows Communication Foundation"
            If Log._LOG_ACTIVO Then Log.escribirLog("NF3a: '" & NF3a & "'", , New StackTrace(0, True))
            Const NF3b As String = "Software\Microsoft\NET Framework Setup\NDP\v3.0\Setup\Windows Workflow Foundation"
            If Log._LOG_ACTIVO Then Log.escribirLog("NF3b: '" & NF3b & "'", , New StackTrace(0, True))
            Const NF3c As String = "Software\Microsoft\NET Framework Setup\NDP\v3.5"
            If Log._LOG_ACTIVO Then Log.escribirLog("NF3c: '" & NF3c & "'", , New StackTrace(0, True))

            Const NF4 As String = "Software\Microsoft\NET Framework Setup\NDP\v4.0"             ' Framework completo
            If Log._LOG_ACTIVO Then Log.escribirLog("NF4: '" & NF4 & "'", , New StackTrace(0, True))
            Const NF4_b As String = "Software\Microsoft\NET Framework Setup\NDP\v4\Client"      ' Client profile
            If Log._LOG_ACTIVO Then Log.escribirLog("NF4_b: '" & NF4_b & "'", , New StackTrace(0, True))
            Const NF4_c As String = "Software\Microsoft\NET Framework Setup\NDP\v4\Full"        ' FULL profile
            If Log._LOG_ACTIVO Then Log.escribirLog("NF4_c: '" & NF4_c & "'", , New StackTrace(0, True))

            Dim InfoVersion As Double = 0
            Dim RegKey As RegistryKey = Nothing

            Try
                If eVersion = Version.NF4 Then
                    Try
                        RegKey = Registry.LocalMachine.OpenSubKey(NF4)
                        If RegKey.GetValue("Install").ToString = "1" Then
                            Return True
                        End If
                    Catch ex As ArgumentException
                        ' No se devuelve nada, hay que realizar la otra comprobacion
                    Catch ex As NullReferenceException
                        ' No se devuelve nada, hay que realizar la otra comprobacion
                    End Try

                    Try
                        RegKey = Registry.LocalMachine.OpenSubKey(NF4_c)
                        If RegKey.GetValue("Install").ToString = "1" Then
                            Return True
                        End If
                    Catch ex As ArgumentException
                        Return False
                    Catch ex As NullReferenceException
                        Return False
                    End Try
                End If

                If eVersion = Version.NF3c Then
                    Try
                        RegKey = Registry.LocalMachine.OpenSubKey(NF3c)
                        If RegKey.GetValue("Install").ToString = "1" Then
                            Return True
                        End If
                    Catch ex As ArgumentException
                        Return False
                    Catch ex As NullReferenceException
                        Return False
                    End Try
                End If


                If eVersion = Version.NF3 Then
                    Try
                        RegKey = Registry.LocalMachine.OpenSubKey(NF3)
                        If RegKey.GetValue("InstallSuccess").ToString = "1" Then
                            Return True
                        End If
                    Catch ex As ArgumentException
                        Return False
                    Catch ex As NullReferenceException
                        Return False
                    End Try
                End If

                If eVersion = Version.NF3a Then
                    Try
                        RegKey = Registry.LocalMachine.OpenSubKey(NF3a)
                        If RegKey.GetValue("InstallSuccess").ToString = "1" Then
                            Return True
                        End If
                    Catch ex As ArgumentException
                        Return False
                    Catch ex As NullReferenceException
                        Return False
                    End Try
                End If

                If eVersion = Version.NF3b Then
                    Try
                        RegKey = Registry.LocalMachine.OpenSubKey(NF3b)
                        If RegKey.GetValue("InstallSuccess").ToString = "1" Then
                            Return True
                        End If
                    Catch ex As ArgumentException
                        Return False
                    Catch ex As NullReferenceException
                        Return False
                    End Try
                End If

                If eVersion = Version.NF2 Then
                    Try
                        RegKey = Registry.LocalMachine.OpenSubKey(NF2)
                        If RegKey.GetValue("Install").ToString = "1" Then
                            Return True
                        End If
                    Catch ex As ArgumentException
                        Return False
                    Catch ex As NullReferenceException
                        Return False
                    End Try
                End If

                If eVersion = Version.NF1 Then
                    Try
                        RegKey = Registry.LocalMachine.OpenSubKey(NF1)
                        If RegKey.GetValue("Install").ToString = "1" Then
                            Return True
                        End If
                    Catch ex As ArgumentException
                        Return False
                    Catch ex As NullReferenceException
                        Return False
                    End Try
                End If

            Catch ex As SecurityException
                Return False
            Finally
                If RegKey IsNot Nothing Then
                    RegKey.Close()
                End If
            End Try

            Return False
        End Function

        Shared Sub New()
            dotNETPath = pNETPath()
            dotNETVersion = pNETVersion()
        End Sub

        ' Funciónes para la comunicación con NET Framework y obtención de información
        <DllImport("mscoree.dll")> _
        Private Shared Function GetCORSystemDirectory( _
            <MarshalAs(UnmanagedType.LPWStr)> ByVal pbuffer As System.Text.StringBuilder, _
            ByVal cchBuffer As Integer, _
            ByRef dwlength As Integer) As Integer
        End Function

        <DllImport("mscoree.dll")> _
        Private Shared Function GetCORVersion( _
            <MarshalAs(UnmanagedType.LPWStr)> ByVal pbuffer As System.Text.StringBuilder, _
            ByVal cchBuffer As Integer, _
            ByRef dwlength As Integer) As Integer
        End Function

        ''' <summary>
        ''' Obtiene la ruta donde está instalado NET Framework
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Shared Function pNETPath() As String
            Dim sb As New System.Text.StringBuilder(255)
            Dim n As Integer
            '
            GetCORSystemDirectory(sb, sb.Capacity, n)
            If n > 0 Then
                Return sb.ToString().Substring(0, n - 1)
            Else
                Return "C:\WINDOWS\Microsoft.NET\Framework\v1.1.4322\"
            End If
        End Function

        ''' <summary>
        ''' Obtiene la versión del NET Framework instalado
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Shared Function pNETVersion() As String
            Dim sb As New System.Text.StringBuilder(255)
            Dim n As Integer
            '
            GetCORVersion(sb, sb.Capacity, n)
            If n > 0 Then
                Return sb.ToString().Substring(0, n - 1)
            Else
                Return "0"
            End If
        End Function
    End Class
End Namespace
