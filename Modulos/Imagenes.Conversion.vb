Imports System.Drawing

Namespace Imagenes
    Namespace Conversion
        Public Module modImagenesConversion
#Region " PIXELS "
            Public Function PixelsToMillimeterX(ByVal ePixels As Single, ByRef g As Graphics) As Single
                Return PixelsToMillimeter(ePixels, g.DpiX)
            End Function

            Public Function PixelsToMillimeterY(ByVal ePixels As Single, ByRef g As Graphics) As Single
                Return PixelsToMillimeter(ePixels, g.DpiY)
            End Function

            Public Function PixelsToCentimeterX(ByVal ePixels As Single, ByRef g As Graphics) As Single
                Return PixelsToCentimeter(ePixels, g.DpiX)
            End Function

            Public Function PixelsToCentimeterY(ByVal ePixels As Single, ByRef g As Graphics) As Single
                Return PixelsToCentimeter(ePixels, g.DpiY)
            End Function

            Public Function PixelsToMillimeter(ByVal ePixels As Single, ByVal eDPI As Single) As Single
                Return ePixels / eDPI * 25.4F
            End Function

            Public Function PixelsToCentimeter(ByVal ePixels As Single, ByVal eDPI As Single) As Single
                Return ((ePixels / eDPI * 25.4F) / 100)
            End Function

            Public Function PixelsToInches(ByVal ePixels As Single, ByVal eDPI As Single) As Single
                Return (ePixels / eDPI)
            End Function
#End Region

#Region " MILIMETROS "
            Public Function MillimetersToPixelsX(ByVal eMM As Single, ByRef g As Graphics) As Single
                Return MillimetersToPixels(eMM, g.DpiX)
            End Function

            Public Function MillimetersToPixelsY(ByVal eMM As Single, ByRef g As Graphics) As Single
                Return MillimetersToPixels(eMM, g.DpiY)
            End Function

            Public Function MillimetersToPixels(ByVal eMM As Single, ByVal eDPI As Single) As Single
                Return eMM / 25.4F * eDPI
            End Function

            Public Function MillimetersToInches(ByVal eMM As Single) As Single
                Return eMM * 25.4F
            End Function
#End Region

#Region " CENTIMETROS "
            Public Function CentimetersToPixelsX(ByVal eCM As Single, ByRef g As Graphics) As Single
                Return CentimetersToPixels(eCM, g.DpiX)
            End Function

            Public Function CentimetersToPixelsY(ByVal eCM As Single, ByRef g As Graphics) As Single
                Return CentimetersToPixels(eCM, g.DpiY)
            End Function

            Public Function CentimetersToPixels(ByVal eCM As Single, ByVal eDPI As Single) As Single
                Return ((eCM / 25.4F * eDPI) * 100)
            End Function

            Public Function CentimetersToInches(ByVal eCM As Single) As Single
                Return ((eCM * 100) / 25.4F)
            End Function
#End Region

#Region " PULGADAS "
            Public Function InchesToPixels(ByVal eInches As Single, ByVal eDPI As Single) As Single
                Return (eInches * eDPI)
            End Function

            Public Function InchesToMilimeters(ByVal eInches As Single) As Single
                Return (eInches * 25.4F)
            End Function

            Public Function InchesToCentimeters(ByVal eInches As Single) As Single
                Return (eInches * 25.4F / 100)
            End Function
#End Region
        End Module
    End Namespace
End Namespace
