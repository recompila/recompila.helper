﻿Imports System.IO
Imports System.Windows.Forms
Imports ICSharpCode.SharpZipLib.Zip

Namespace Compresores
#Region "Modulos"
    Public Module modCompresores
        ''' <summary>
        ''' Comprime los archivos que se le pasan como parametro. Si se establece el parametro
        ''' crearAuto a True, se establecerá automaticamente el nombre del fichero a partir de
        ''' la feha actual, en formato ZIPyyMMddHHmmss.zip y lo devuelve como resultado
        ''' Si la lista de archivos a comprimir está vacia, se generará una excepción.
        ''' Puede comprimir subdirectorios, no se considerará un error si  hay  subdirectorios
        ''' vacios, es decir, estos se comprimiran en el archivo resultante, aunque no contengan
        ''' archivos
        ''' </summary>
        ''' <param name="eFicherosEntrada">Array de archivos a ser comprimidos pasandole la ruta completa</param>         
        ''' <param name="eRutaBase">Ruta a partir de la cual se considera raiz del directorio, en blanco utilizar ruta completa</param>   
        ''' <param name="eBarraProgreso">Si se quiere mostrar el proceso en una ProgressBar esta se le pasa como parámetro</param>
        ''' <param name="eLabelEstado">Si se quiere mostrar el estado en un Label este se le pasa como parámetro</param>
        ''' <param name="eFicheroSalida">Ruta del fichero de salida con los comprimidos (Si no se le pasa se genera automáticamente)</param>
        ''' <param name="eNivelCompresion">Nivel de compresión a utilizar, entre 0 y 9</param>                        
        ''' <param name="eSobreescribirDestino">Determina si se tiene que sobreescribir el archivo de destino si existe</param>
        ''' <returns>True | False dependiendo del resultado de la ejecución</returns>
        Public Function comprimirFicheros(ByVal eFicherosEntrada As List(Of String), _
                                          ByVal eRutaBase As String, _
                                          Optional ByRef eBarraProgreso As Object = Nothing, _
                                          Optional ByRef eLabelEstado As Object = Nothing, _
                                          Optional ByVal eFicheroSalida As String = "", _
                                          Optional ByVal eNivelCompresion As Integer = 6, _
                                          Optional ByVal eSobreescribirDestino As Boolean = True) As Boolean
            ' Objeto utilizado para comprimir los ficheros de entrada
            Dim elZipSalida As ZipOutputStream = Nothing
            Dim paraDevolver As Boolean = True

            ' Si no hay ficheros para comprimir se lanza una excepción indicando el problema
            If eFicherosEntrada.Count = 0 Then Throw New Exception("No hay archivos que comprimir...", Nothing)

            ' Si existe el destino se elimina antes de continuar si está así establecido
            ' en el parámetro eSobreescribirDestino que controla este comportamiento
            If Not String.IsNullOrEmpty(eFicheroSalida) AndAlso File.Exists(eFicheroSalida) Then
                If eSobreescribirDestino Then
                    Try
                        File.Delete(eFicheroSalida)
                    Catch ex As Exception
                        Throw New Exception("No se ha podido eliminar el fichero de salida.", ex)
                    End Try
                End If
            End If

            ' Si no se establece un nombre para el fichero, este se asignará de forma automática,
            ' utilizando como path de salida el directorio donde se está ejecutando la aplicación
            If String.IsNullOrEmpty(eFicheroSalida) Then eFicheroSalida = ".\ZIP" & DateTime.Now.ToString("yyMMddHHmmss") & ".zip"

            ' Se realiza la compresión de los archivos de entrada
            Try
                If eBarraProgreso IsNot Nothing Then WinForms.ProgressBar.fijarMaximoBarra(eBarraProgreso, eFicherosEntrada.Count)
                If eLabelEstado IsNot Nothing Then eLabelEstado.Text = "Realizando operaciones previas..."

                ' Se crean los objetos necesarios para realizar las operaciones
                ' y se calcula la longitud de la ruta base donde se encuentran los
                ' archivos a comprimir
                Dim longitudRutaBase As Integer = 0
                If Not String.IsNullOrEmpty(eRutaBase) Then
                    If eRutaBase.EndsWith("\") Then
                        longitudRutaBase = eRutaBase.Length
                    Else
                        longitudRutaBase = (eRutaBase.Length + 1)
                    End If
                End If

                ' Se crea el objeto para realizar la compresión y se asignan los parametros necesarios,
                ' nivel de compresión, password, etc.
                elZipSalida = New ZipOutputStream(File.Open(eFicheroSalida, FileMode.Create))
                elZipSalida.SetLevel(eNivelCompresion)

                ' Se recorren los ficheros de entrada y se añaden al fichero
                ' de comprimido de salida
                For Each unFichero In eFicherosEntrada
                    Try
                        If eBarraProgreso IsNot Nothing Then WinForms.ProgressBar.AumentarBarra(eBarraProgreso)
                        If eLabelEstado IsNot Nothing Then eLabelEstado.Text = "Comprimiendo " & unFichero & "..."

                        If System.IO.File.Exists(unFichero) Then
                            ' Se lee el conetenido de cada fichero
                            Dim elStreamFichero As FileStream = File.OpenRead(unFichero)
                            Dim elBuffer(Convert.ToInt32(elStreamFichero.Length - 1)) As Byte
                            elStreamFichero.Read(elBuffer, 0, elBuffer.Length)

                            ' Se elimina la rutaBase para evitar que al  descomprimir  los  archivos  se
                            ' copien utilizando esta, y evitando así los errores que  se pudieran produ-
                            ' cir si hay dos archivos con el mismo nombre en distintos directorios.
                            Dim rutaParcialFichero As String = unFichero.Substring(longitudRutaBase)
                            Dim laEntradaZip As ZipEntry = New ZipEntry(rutaParcialFichero)

                            ' Se establece la información sobre la fecha y tamaño del
                            ' archivo que se está añadiendo al ZIP
                            laEntradaZip.DateTime = New FileInfo(unFichero).LastWriteTime
                            laEntradaZip.Size = elStreamFichero.Length

                            ' Se añade el fichero comprimido al archivo de salida
                            elStreamFichero.Close()
                            elZipSalida.PutNextEntry(laEntradaZip)
                            elZipSalida.Write(elBuffer, 0, elBuffer.Length)
                        End If
                    Catch ex As Exception
                        If Log._LOG_ACTIVO Then Log.escribirLog("ERROR. Al comprimir/añadir el fichero '" & unFichero & "' al archivo comprimido '" & eFicheroSalida & "'.", ex, New StackTrace(0, True))
                    End Try
                Next

                If eLabelEstado IsNot Nothing Then eLabelEstado.Text = "Finalizando operaciones..."
            Catch ex As Exception
                Dim elMensajeError As String = "ERROR. Se ha producido un error al comprimir los ficheros en '" & eFicheroSalida & "'."
                If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeError, ex, New StackTrace(0, True))
                Throw New Exception(elMensajeError, ex)
                paraDevolver = False
            Finally
                If elZipSalida IsNot Nothing Then
                    elZipSalida.Finish()
                    elZipSalida.Close()
                    elZipSalida = Nothing
                End If
            End Try

            Return (paraDevolver AndAlso (IO.File.Exists(eFicheroSalida)))
        End Function

        ''' <summary>
        ''' Comprime la carpeta que se le pasa como parámetro de forma recursiva
        ''' </summary>
        ''' <param name="eCarpetaEntrada">Carpeta a ser comprimida</param>
        ''' <param name="eBarraProgreso">Si se quiere mostrar el proceso en una ProgressBar esta se le pasa como parámetro</param>
        ''' <param name="eLabelEstado">Si se quiere mostrar el estado en un Label este se le pasa como parámetro</param>
        ''' <param name="eFicheroSalida">Ruta del fichero de salida con los comprimidos (Si no se le pasa se genera automáticamente)</param>
        ''' <param name="eNivelCompresion">Nivel de compresión a utilizar, entre 0 y 9</param>                        
        ''' <param name="eSobreescribirDestino">Determina si se tiene que sobreescribir el archivo de destino si existe</param>
        ''' <returns>True | False dependiendo del resultado de la ejecución</returns>
        Public Function comprimirCarpeta(ByVal eCarpetaEntrada As String, _
                                         Optional ByRef eBarraProgreso As Object = Nothing, _
                                         Optional ByRef eLabelEstado As Object = Nothing, _
                                         Optional ByVal eFicheroSalida As String = "", _
                                         Optional ByVal eNivelCompresion As Integer = 6, _
                                         Optional ByVal eSobreescribirDestino As Boolean = True) As Boolean

            If Not Directory.Exists(eCarpetaEntrada) Then Throw New Exception("No existe la carpeta de entrada a comprimir.", Nothing)

            ' Se obtienen todos los archivos y carpetas existentes en la carpeta base
            ' desde la que se van a comprimir los archivos
            Dim InfoCarpeta As New DirectoryInfo(eCarpetaEntrada)
            Dim ListaArchivos As New List(Of String)
            For Each Archivo As FileInfo In InfoCarpeta.GetFiles("*.*", SearchOption.AllDirectories)
                ListaArchivos.Add(Archivo.FullName)
            Next

            ' Se le pasa la lista de archivos a la función de comprimir archivos
            Return (comprimirFicheros(ListaArchivos, _
                                      eCarpetaEntrada, _
                                      eBarraProgreso, _
                                      eLabelEstado, _
                                      eFicheroSalida, _
                                      eNivelCompresion, _
                                      eSobreescribirDestino))
        End Function

        ''' <summary>
        ''' Descomprime los archivos del fichero que se le pasa como parametro de entrada en
        ''' la carpeta que se le pasa para se extraídos
        ''' </summary>
        ''' <param name="eFicheroEntrada">Fichero ZIP a descomprimir</param>
        ''' <param name="eCarpetaSalida">Directorio donde se deben descomprimir los archivos del fichero de entrada</param>   
        ''' <param name="eBarraProgreso">Si se quiere mostrar el proceso en una ProgressBar esta se le pasa como parámetro</param>
        ''' <param name="eLabelEstado">Si se quiere mostrar el estado en un Label este se le pasa como parámetro</param>
        ''' <param name="eBorrarFicheroEntrada">Una vez finalizado la descompresión, este se borra</param>
        ''' <returns>True o False dependiendo del resultado de la ejecución</returns>
        Public Function descomprimirFicheros(ByVal eFicheroEntrada As String, _
                                             Optional ByVal eCarpetaSalida As String = "", _
                                             Optional ByRef eBarraProgreso As Object = Nothing, _
                                             Optional ByRef eLabelEstado As Object = Nothing, _
                                             Optional ByVal eBorrarFicheroEntrada As Boolean = False) As Boolean
            ' Objeto utilizado para descomprimir los ficheros de entrada
            Dim elZipEntrada As ZipInputStream = Nothing
            Dim paraDevolver As Boolean = True

            ' Primero se comprueba que existe el fichero a descomprimir, de no ser así, se lanza una excepción
            ' indicando el problema ya que no se puede continuar
            If Not System.IO.File.Exists(eFicheroEntrada) Then Throw New Exception("No existe el fichero " & eFicheroEntrada & " o este no es accesible.", Nothing)

            ' Se crea y configura el Stream para la descompresión del archivo ZIP
            If eLabelEstado IsNot Nothing Then eLabelEstado.Text = "Realizando operaciones previas..."

            Try
                ' Objetos utilizados para la descopmresión de los archivos contenidos
                elZipEntrada = New ZipInputStream(File.OpenRead(eFicheroEntrada))
                Dim laEntrada As ZipEntry = Nothing

                ' Se cuentan las entradas que hay en el archivo comprimido
                ' para poder mostrar el progreso
                If eBarraProgreso IsNot Nothing Then
                    Dim contadorFicheros As Long = 0
                    While (True)
                        laEntrada = elZipEntrada.GetNextEntry()
                        contadorFicheros += 1
                        If laEntrada Is Nothing Then Exit While
                    End While

                    WinForms.ProgressBar.fijarMaximoBarra(eBarraProgreso, contadorFicheros)
                End If

                ' Se crea el objeto y ZipInputStream y se lee la cabecera del archivo a descomprimir
                elZipEntrada = New ZipInputStream(File.OpenRead(eFicheroEntrada))

                ' Si no existe el directorio de salida, este se va a crear, teniendo en cuenta que
                ' la omisión de este parametro descomprimira los archivos en el mismo directorio
                If Not String.IsNullOrEmpty(eCarpetaSalida) AndAlso eCarpetaSalida <> "." Then
                    If Not Directory.Exists(eCarpetaSalida) Then
                        Try
                            Directory.CreateDirectory(eCarpetaSalida)
                        Catch ex As Exception
                            Dim elMensajeError As String = "ERROR. No se pudo crear el directorio '" & eCarpetaSalida & "' para descomprimir el archivo '" & eFicheroEntrada & "'..."
                            If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeError, ex, New StackTrace(0, True))
                            Throw New Exception(elMensajeError, ex)
                            paraDevolver = False
                        End Try
                    End If
                Else
                    eCarpetaSalida = "."
                End If

                ' Se añade la \ a la carpeta de salida si esta no estaba configurada
                If Not eCarpetaSalida.EndsWith("\") Then eCarpetaSalida &= "\"

                If IO.Directory.Exists(eCarpetaSalida) Then
                    While (True)
                        If eBarraProgreso IsNot Nothing Then WinForms.ProgressBar.AumentarBarra(eBarraProgreso)

                        ' Se obtiene el sigunte elemento del archivo a descomprimir, si es Nothing
                        ' se considera que llegó al final por lo que se sale del bucle
                        laEntrada = elZipEntrada.GetNextEntry()
                        If laEntrada Is Nothing Then Exit While

                        Dim nombreFichero As String = laEntrada.Name.Trim
                        If eLabelEstado IsNot Nothing Then eLabelEstado.Text = "Descomprimiendo " & nombreFichero & "..."

                        If Not String.IsNullOrEmpty(nombreFichero) Then
                            ' Se determina si se trata de una carpeta o un fichero, en el caso
                            ' de una carpeta esta se tiene que crear en el disco
                            If nombreFichero.EndsWith("\") Or nombreFichero.EndsWith("/") Then
                                Dim rutaSubCarpeta As String = String.Empty
                                Try
                                    rutaSubCarpeta = eCarpetaSalida & laEntrada.Name
                                    If Not IO.Directory.Exists(rutaSubCarpeta) Then Directory.CreateDirectory(rutaSubCarpeta)
                                Catch ex As Exception
                                    Dim elMensajeError As String = "No se ha podido crear el directorio " & rutaSubCarpeta & " al descomprimir '" & eFicheroEntrada & "'"
                                    If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeError, ex, New StackTrace(0, True))
                                    Throw New Exception(elMensajeError, ex)
                                    paraDevolver = False
                                End Try
                            Else
                                ' Se trata de un fichero, por lo que se obtiene su ruta completa de salida
                                ' y la carpeta donde se tiene que desocmprimir
                                Dim rutaFichero As String = eCarpetaSalida & laEntrada.Name.Replace("/", "\").Replace("\ ", "\")
                                Dim rutaCarpetaContenedora As String = Ficheros.extraerRutaFichero(rutaFichero)

                                ' Si todavía no existe la carepta de salida, esta se crea
                                If Not Directory.Exists(rutaCarpetaContenedora) Then
                                    Try
                                        Directory.CreateDirectory(rutaCarpetaContenedora)
                                    Catch ex As Exception
                                        Dim elMensajeError As String = "ERROR. No se ha podido crear el directorio " & rutaCarpetaContenedora & " al descomprimir '" & eFicheroEntrada & "'"
                                        If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeError, ex, New StackTrace(0, True))
                                        Throw New Exception(elMensajeError, ex)
                                        paraDevolver = False
                                    End Try
                                End If

                                ' Se descomprime el fichero en la carpeta de salida
                                Try
                                    Dim elEscritor As FileStream = File.Create(rutaFichero)
                                    Dim elTamanho As Integer = 2048
                                    Dim losDatos As Byte() = New Byte(2047) {}

                                    While True
                                        elTamanho = elZipEntrada.Read(losDatos, 0, losDatos.Length)
                                        If elTamanho > 0 Then
                                            elEscritor.Write(losDatos, 0, elTamanho)
                                        Else
                                            Exit While
                                        End If
                                    End While

                                    elEscritor.Close()
                                Catch ex As Exception
                                    Dim elMensajeError As String = "ERROR. No se ha podido descromprimir el fichero " & rutaFichero
                                    If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeError, ex, New StackTrace(0, True))
                                    Throw New Exception(elMensajeError, ex)
                                    paraDevolver = False
                                End Try
                            End If
                        End If
                    End While
                Else
                    Dim elMensajeError As String = "ERROR. No existe la carpeta de salida en '" & eCarpetaSalida & "' para descomprimir el archivo '" & eFicheroEntrada & "'..."
                    If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeError, , New StackTrace(0, True))
                    Throw New Exception(elMensajeError, Nothing)
                    paraDevolver = False
                End If
            Catch ex As Exception
                Dim elMensajeError As String = "ERROR. Se ha producido un error al descomprimir los ficheros de '" & eFicheroEntrada & "'."
                If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeError, ex, New StackTrace(0, True))
                Throw New Exception(elMensajeError, ex)
                paraDevolver = False
            Finally
                If elZipEntrada IsNot Nothing Then
                    elZipEntrada.Close()
                    elZipEntrada.Dispose()
                    elZipEntrada = Nothing
                End If
            End Try

            If eLabelEstado IsNot Nothing Then eLabelEstado.Text = "Finalizando operaciones..."

            If eBorrarFicheroEntrada Then
                Try
                    File.Delete(eFicheroEntrada)
                Catch ex As Exception
                    Dim elMensajeError As String = "ERROR. No se ha podido eliminar el fichero de entrada a descomprimir " & eFicheroEntrada
                    If Log._LOG_ACTIVO Then Log.escribirLog(elMensajeError, ex, New StackTrace(0, True))
                    Throw New Exception(elMensajeError, ex)
                    paraDevolver = False
                End Try
            End If

            Return True
        End Function
    End Module
#End Region
End Namespace

