﻿' Para utilizar esta librería es necesario añadir las referencias a:
'   *   System.Web.Services

Imports System.CodeDom
Imports System.CodeDom.Compiler
Imports System.Security.Permissions
Imports System.Web.Services.Description
Imports System.Reflection

Namespace ServiciosWeb
    Public Module ModServiciosWeb

        Public Function WebServiceOnline(ByVal webServiceAsmxUrl As String) As Boolean
            Dim stream As System.IO.Stream = Nothing
            Try
                Dim client As System.Net.WebClient = New System.Net.WebClient()

                '-Connect To the web service
                stream = client.OpenRead(webServiceAsmxUrl + "?wsdl")

                Return stream IsNot Nothing
            Catch ex As Exception
                Return False
            Finally
                If stream IsNot Nothing Then
                    stream.Close()
                    stream.Dispose()
                End If
            End Try
        End Function

        Public Function CallWebService(ByVal webServiceAsmxUrl As String, ByVal serviceName As String, ByVal methodName As String, ByVal ParamArray args As Object()) As Object

            Try
                Dim client As System.Net.WebClient = New System.Net.WebClient()

                '-Connect To the web service
                Dim stream As System.IO.Stream = client.OpenRead(webServiceAsmxUrl + "?wsdl")

                'Read the WSDL file describing a service.
                Dim description As ServiceDescription = ServiceDescription.Read(stream)

                'LOAD THE DOM'''''''''''''''''''''''''''

                '--Initialize a service description importer.
                Dim importer As ServiceDescriptionImporter = New ServiceDescriptionImporter()
                importer.ProtocolName = "Soap" ' Use SOAP 1.2.
                importer.AddServiceDescription(description, Nothing, Nothing)

                '--Generate a proxy client. 

                importer.Style = ServiceDescriptionImportStyle.Client
                '--Generate properties to represent primitive values.
                importer.CodeGenerationOptions = System.Xml.Serialization.CodeGenerationOptions.GenerateProperties

                'Initialize a Code-DOM tree into which we will import the service.
                Dim nmspace As CodeNamespace = New CodeNamespace()
                Dim unit1 As CodeCompileUnit = New CodeCompileUnit()
                unit1.Namespaces.Add(nmspace)

                'Import the service into the Code-DOM tree. 
                'This creates proxy code that uses the service.

                Dim warning As ServiceDescriptionImportWarnings = importer.Import(nmspace, unit1)

                If warning = 0 Then

                    '--Generate the proxy code
                    Dim provider1 As CodeDomProvider = _
                              CodeDomProvider.CreateProvider("VB")
                    '--Compile the assembly proxy with the // appropriate references
                    Dim assemblyReferences() As String
                    assemblyReferences = New String() {"System.dll", _
                        "System.Web.Services.dll", "System.Web.dll", _
                        "System.Xml.dll", "System.Data.dll"}
                    Dim parms As CompilerParameters = New CompilerParameters(assemblyReferences)
                    parms.GenerateInMemory = True '(Thanks for this line nikolas)
                    Dim results As CompilerResults = provider1.CompileAssemblyFromDom(parms, unit1)

                    '-Check For Errors
                    If results.Errors.Count > 0 Then

                        Dim oops As CompilerError
                        For Each oops In results.Errors
                            System.Diagnostics.Debug.WriteLine("========Compiler error============")
                            System.Diagnostics.Debug.WriteLine(oops.ErrorText)
                        Next
                        Throw New System.Exception("Compile Error Occurred calling webservice.")
                    End If

                    '--Finally, Invoke the web service method
                    Dim wsvcClass As Object = results.CompiledAssembly.CreateInstance(serviceName)
                    Dim mi As MethodInfo = wsvcClass.GetType().GetMethod(methodName)
                    Return mi.Invoke(wsvcClass, args)

                Else
                    Return Nothing
                End If
            Catch ex As Exception
                If Log._LOG_ACTIVO Then Log.escribirLog("ERROR al invocar el método " & methodName & " del servicio web " & serviceName & "...", ex, New StackTrace(0, True))
                Return Nothing
            End Try
        End Function
    End Module
End Namespace
