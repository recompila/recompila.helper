﻿Imports System.IO
Imports System.Reflection
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Xml.Serialization
Imports System.Text

Namespace Serializacion
    Public Module modSerializacion

        ''' <summary> 
        ''' To convert a Byte Array of Unicode values (UTF-8 encoded) to a complete String. 
        ''' </summary> 
        ''' <param name="characters">Unicode Byte Array to be converted to String</param> 
        ''' <returns>String converted from Unicode Byte Array</returns> 
        Public Function UTF8ByteArrayToString(ByVal characters As Byte()) As String
            Dim encoding As New UTF8Encoding
            Dim constructedString As String = encoding.GetString(characters)
            Return constructedString
        End Function

        ''' <summary> 
        ''' Converts the String to UTF8 Byte array and is used in De serialization 
        ''' </summary> 
        ''' <param name="pXmlString"></param> 
        ''' <returns></returns> 
        Public Function StringToUTF8ByteArray(ByVal pXmlString As String) As Byte()
            Dim encoding As New UTF8Encoding()
            Dim byteArray As Byte() = encoding.GetBytes(pXmlString)
            Return byteArray
        End Function

        ''' <summary>
        ''' Serializa un objeto como un array de bytes para poder trabajar con el stream
        ''' </summary>
        ''' <param name="eObjeto">Objeto que se desea serializar</param>
        ''' <returns>El objeto serializado en un stream</returns>
        Public Function serializarObjeto2Stream(ByVal eObjeto As Object) As Stream
            Dim paraDevolver As New MemoryStream

            Try
                Dim elSerializador As BinaryFormatter = New BinaryFormatter()
                elSerializador.Serialize(paraDevolver, eObjeto)
                paraDevolver.Position = 0
            Catch ex As Exception
                If Log._LOG_ACTIVO Then Log.escribirLog("Error de serialización...", ex, New StackTrace(0, True))
                paraDevolver = Nothing
            End Try

            Return paraDevolver
        End Function

        ''' <summary>
        ''' Genera un objeto a partir del propio objeto serializado en un stream
        ''' </summary>
        ''' <param name="eStream">Stream con los datos de la serializacion</param>
        ''' <returns>El objeto desserializado</returns>
        Public Function desSerializarStream2Objeto(ByVal eStream As Stream) As Object
            If eStream Is Nothing OrElse eStream.Length = 0 Then Return Nothing

            Try                
                Dim elSerializador As BinaryFormatter = New BinaryFormatter()
                eStream.Position = 0
                Return elSerializador.Deserialize(eStream)
            Catch ex As Exception
                If Log._LOG_ACTIVO Then Log.escribirLog("Error de des-serialización...", ex, New StackTrace(0, True))
                Return Nothing
            End Try
        End Function

    ''' <summary> 
        ''' Serialize an object into an XML string 
        ''' </summary> 
        ''' <typeparam name="T"></typeparam> 
        ''' <param name="obj"></param> 
        ''' <returns></returns> 
        Public Function serializarObjeto2XML_Stream(Of T)(ByVal obj As t) As Stream
            Try
                Dim xmlString As String = Nothing

                Dim memoryStream As New MemoryStream()
                Dim xs As New XmlSerializer(GetType(T))
                Dim xmlTextWriter As New Xml.XmlTextWriter(memoryStream, Encoding.UTF8)
                xs.Serialize(xmlTextWriter, obj)
                memoryStream = CType(xmlTextWriter.BaseStream, MemoryStream)
                memoryStream.Position = 0
                Return memoryStream
            Catch
                Return Nothing
            End Try
        End Function

        ''' <summary> 
        ''' Serialize an object into an XML string 
        ''' </summary> 
        ''' <typeparam name="T"></typeparam> 
        ''' <param name="obj"></param> 
        ''' <returns></returns> 
        Public Function serializarObjeto2XML_String(Of T)(ByVal obj As t) As String
            Try
                Dim xmlString As String = Nothing

                Dim memoryStream As New MemoryStream()
                Dim xs As New XmlSerializer(GetType(T))
                Dim xmlTextWriter As New Xml.XmlTextWriter(memoryStream, Encoding.UTF8)
                xs.Serialize(xmlTextWriter, obj)
                memoryStream = CType(xmlTextWriter.BaseStream, MemoryStream)
                xmlString = UTF8ByteArrayToString(memoryStream.ToArray())
                memoryStream.Dispose()

                Return xmlString
            Catch
                Return String.Empty
            End Try
        End Function

        ''' <summary> 
        ''' Reconstruct an object from an XML string 
        ''' </summary> 
        ''' <param name="eStream"></param> 
        ''' <returns></returns> 
        Public Function deSerializarXML_Stream2Objeto(Of T)(ByVal eStream As Stream) As T
            Dim xs As New XmlSerializer(GetType(T))
            Dim xmlTextWriter As New System.Xml.XmlTextWriter(eStream, Encoding.UTF8)

            Dim theObject As T = CType(xs.Deserialize(eStream), T)
            eStream.Dispose()
            Return theObject

        End Function

        ''' <summary>
        ''' Serializa un objeto y lo almacena en un fichero
        ''' </summary>
        ''' <param name="eObjeto">Objeto a serializar</param>
        ''' <param name="eRutaFichero">Ruta del fichero donde se va a guardar</param>
        ''' <returns>True / False dependiendo de si se completó o no la operación</returns>
        Public Function serializarObjeto2Fichero(ByVal eObjeto As Object, _
                                                 ByVal eRutaFichero As String) As Boolean

            If File.Exists(eRutaFichero) Then
                Try
                    File.Delete(eRutaFichero)
                Catch ex As Exception
                    If Log._LOG_ACTIVO Then Log.escribirLog("Error de escritura en " & eRutaFichero & "...", ex, New StackTrace(0, True))
                    Return False
                End Try
            End If

            Try
                Dim elEscritor As FileStream = File.Create(eRutaFichero)
                Dim elSerializador As BinaryFormatter = New BinaryFormatter()
                elSerializador.Serialize(elEscritor, eObjeto)
                elEscritor.Close()
            Catch ex As Exception
                If Log._LOG_ACTIVO Then Log.escribirLog("Error de serialización...", ex, New StackTrace(0, True))
                Return False
            End Try

            Return True
        End Function

        ''' <summary>
        ''' Deserializa un objeto que se almacenó en un fichero
        ''' </summary>
        ''' <param name="eRutaFichero">Ruta del fichero a deserializar</param>
        ''' <returns>El propio objeto deserializado</returns>
        Public Function desSerializarFichero2Objeto(ByVal eRutaFichero As String) As Object
            Dim paraDevolver As Object = Nothing

            If File.Exists(eRutaFichero) Then
                Try
                    Dim elLector As FileStream = File.OpenRead(eRutaFichero)
                    Dim elSerializador As BinaryFormatter = New BinaryFormatter()
                    paraDevolver = elSerializador.Deserialize(elLector)
                    elLector.Close()
                Catch ex As Exception
                    If Log._LOG_ACTIVO Then Log.escribirLog("Error de des-serialización...", ex, New StackTrace(0, True))
                    Return Nothing
                End Try
            End If

            Return paraDevolver
        End Function
    End Module
End Namespace