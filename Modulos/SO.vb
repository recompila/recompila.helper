﻿' Para utilizar esta librería es necesario añadir las referencias a:
'   *   System.Management

Imports System.Text
Imports System.Management
Imports System.Runtime.InteropServices
Imports System.Windows.Forms

Namespace SistemasOperativos
    Public Module Procesadoares
        ''' <summary>
        ''' Obtiene la arquitectura del procesador (X86 | X64)
        ''' </summary>
        ''' <returns>Arquitectura del procesador</returns>
        Public Function ArquitecturaProcesador() As String
            Dim paraDevolver As String = ""

            Try
                Dim laConsulta As String = "SELECT * FROM Win32_processor"
                Dim elScope As ManagementScope = New ManagementScope(String.Format("\\{0}\root\cimv2", System.Environment.MachineName), New ConnectionOptions)
                Dim elObjeto As ObjectQuery = New ObjectQuery(laConsulta)
                Dim queryCollection As ManagementObjectCollection = New ManagementObjectSearcher(elScope, elObjeto).Get

                For Each Elemento In queryCollection
                    paraDevolver = Elemento("AddressWidth").ToString()
                Next
            Catch ex As Exception
                paraDevolver = "x86"
            End Try

            Return paraDevolver
        End Function

        ''' <summary>
        ''' Obtiene el número de cores del procesador
        ''' </summary>
        ''' <returns>Numero de cores del procesador</returns>
        Public Function NumeroProcesadores() As Integer
            Dim paraDevolver As Integer = 0

            Try
                Dim laConsulta As String = "SELECT * FROM Win32_ComputerSystem"
                Dim elObjeto As New ManagementObjectSearcher(laConsulta)
                For Each unProcesador As ManagementObject In elObjeto.Get()
                    Try
                        paraDevolver += Funciones.NZI(unProcesador.Properties("NumberOfLogicalProcessors").Value.ToString())
                    Catch ex As Exception
                    End Try
                Next unProcesador
            Catch ex As Exception
                paraDevolver = 2
            End Try

            If paraDevolver = 0 Then paraDevolver = 1

            Return paraDevolver
        End Function

        ''' <summary>
        ''' Determina si la resolución del equipo es mayor que la resolución que se le pasa por parámetros
        ''' </summary>
        Public Function resolucionMayor(ByVal eAncho As Long, ByVal eAlto As Long) As Boolean
            Dim screenWidth As Integer = Screen.PrimaryScreen.Bounds.Width
            Dim screenHeight As Integer = Screen.PrimaryScreen.Bounds.Height

            Return ((screenWidth >= eAncho) And (screenHeight >= eAlto))
        End Function
    End Module

    Public Module SO
        ''' <summary>
        ''' Versiones de Windows
        ''' </summary>
        Public Enum enmVersionWindows
            Windows31
            Windows95
            Windows98
            Windows98SE
            WindowsME
            WindowsNT351
            WindowsNT40
            Windows2000
            WindowsXP
            Windows2003
            Windows2008
            WindowsVista
            Windows7
            WindowsCE
            Unix
            Desconocido
        End Enum

        <DllImport("kernel32.dll")> _
        Private Function GetSystemWow64Directory(ByVal lpBuffer As StringBuilder, ByVal uSize As UInteger) As UInteger
        End Function

        Public Function getOSPlatForm() As String
            Dim lpBuffer As New StringBuilder(260)
            Dim uSize As UInteger = GetSystemWow64Directory(lpBuffer, 260)
            If (uSize > 0) Then
                Return "WIN_64"
            Else
                Return "WIN_32"
            End If
        End Function

        Public Function esWin64() As Boolean
            Dim lpBuffer As New StringBuilder(260)
            Dim uSize As UInteger = GetSystemWow64Directory(lpBuffer, 260)
            Return (uSize > 0)
        End Function

        Public Function ObtenerSistemaOperativo() As enmVersionWindows
            Select Case Environment.OSVersion.Platform
                Case PlatformID.Win32S
                    Return enmVersionWindows.Windows31
                Case PlatformID.Win32Windows
                    Select Case Environment.OSVersion.Version.Minor
                        Case 0
                            Return enmVersionWindows.Windows95
                        Case 10
                            If Environment.OSVersion.Version.Revision.ToString() = "2222A" Then
                                Return enmVersionWindows.Windows98
                            Else
                                Return enmVersionWindows.Windows98SE
                            End If
                        Case 90
                            Return enmVersionWindows.WindowsME
                    End Select
                Case PlatformID.Win32NT
                    Select Case Environment.OSVersion.Version.Major
                        Case 3
                            Return enmVersionWindows.WindowsNT351
                        Case 4
                            Return enmVersionWindows.WindowsNT40
                        Case 5
                            Select Case Environment.OSVersion.Version.Minor
                                Case 0
                                    Return enmVersionWindows.Windows2000
                                Case 1
                                    Return enmVersionWindows.WindowsXP
                                Case 2
                                    Return enmVersionWindows.Windows2003
                            End Select
                        Case 6
                            Select Case Environment.OSVersion.Version.Minor
                                Case 0
                                    Return enmVersionWindows.WindowsVista
                                Case 1
                                    If Environment.OSVersion.Version.Build >= 7600 Then
                                        Return enmVersionWindows.Windows7
                                    Else
                                        Return enmVersionWindows.Windows2008
                                    End If
                                Case 2
                                    Return enmVersionWindows.Windows7
                            End Select
                    End Select
                Case PlatformID.WinCE
                    Return enmVersionWindows.WindowsCE
                Case PlatformID.Unix
                    Return enmVersionWindows.Unix
            End Select

            ' Si no encontró nada, devuelvo desconocido
            Return enmVersionWindows.Desconocido
        End Function
    End Module
End Namespace