﻿Imports System.Threading
Imports System.Windows.Forms

Namespace Windows
    Namespace FireWall
#Region " ESTRUCTURAS PUBLICAS "
        Public Structure strDatosAplicacion
            Public Property NombreAplicacion As String
            Public Property ReglaActiva As Boolean
            Public Property IPVersion As String
            Public Property RutaEjecutable As String
            Public Property IPRemota As String
            Public Property Scope As String
        End Structure

        Public Structure strDatosPuerto
            Public Property NombrePuerto As String
            Public Property Puerto As Integer
            Public Property ReglaActiva As Boolean
            Public Property IPVersion As String

            Public Property Protocolo As String
            Public Property Scope As String
            Public Property IPRemota As String
            Public Property BuiltIn As Boolean
        End Structure
#End Region

        Public Class cFirewall
            Implements IDisposable

#Region " CONSTANTES "
            ' Sobre que perfil se va a actuar
            Private Const NET_FW_PROFILE_DOMAIN = 0
            Private Const NET_FW_PROFILE_STANDARD = 1

            ' Sobre que red se va a actuar
            Private Const NET_FW_SCOPE_ALL = 0
            Private Const NET_FW_SCOPE_LOCAL_SUBNET = 1

            ' Tipo de versión IP que se va a utilizar (ANY son IPv4 y IPv6)
            Private Const NET_FW_IP_VERSION_ANY = 2

            ' Ids para identificar los protocolos
            Private Const NET_FW_IP_PROTOCOL_UDP = 17
            Private Const NET_FW_IP_PROTOCOL_TCP = 6

            ''' <summary>
            ''' Comunicación con el Fireall
            ''' </summary>
            Private Shared i_OJBETOS_FIREWALL As strObjetoFirewall = Nothing
#End Region

#Region " PROPIEDADES "
            ''' <summary>
            ''' Comunicación con el fireall
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Private Shared ReadOnly Property _OJBETOS_FIREWALL As strObjetoFirewall
                Get
                    Return i_OJBETOS_FIREWALL
                End Get
            End Property

            ''' <summary>
            ''' Determina si se puede comunicar con el Fireall
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public ReadOnly Property puedeComunicarse As Boolean
                Get
                    Return i_OJBETOS_FIREWALL IsNot Nothing
                End Get
            End Property
#End Region

#Region " ESTRUCTURAS "
            ''' <summary>
            ''' Objeto para manejar el FireWall y el Perfil del Firewall configurado
            ''' </summary>
            Private Class strObjetoFirewall
                Public Property elFirewall As Object
                Public Property laPolitica As Object
            End Class
#End Region
#Region " CONSTRUCTORES "
            Public Sub New(ByVal eTiempoEspera As Short)
                obtenerConfiguradorFirewall(eTiempoEspera)
            End Sub
#End Region

#Region " METODOS INTERNOS "
            Private Sub obtenerConfiguradorFirewallInterno()
                i_OJBETOS_FIREWALL = New strObjetoFirewall
                Try
                    i_OJBETOS_FIREWALL.elFirewall = CreateObject("HNetCfg.FwMgr")
                    i_OJBETOS_FIREWALL.laPolitica = _OJBETOS_FIREWALL.elFirewall.LocalPolicy.CurrentProfile
                Catch ex As Exception
                    If Log._LOG_ACTIVO Then Log.escribirLog("ERROR al crear el manejador del Firewall de Windows y/o el perfil actual.", ex, New StackTrace(0, True))
                    i_OJBETOS_FIREWALL = Nothing
                End Try
            End Sub

            Private Function obtenerConfiguradorFirewall(ByVal eTiempoEspera As Short) As strObjetoFirewall
                ' Todas las funciones tienen que obtener un objeto de configuración
                ' del Firewal, pero a veces, esta obtención no se produce correctamente
                ' debido a que el propio firewall las bloquea, o que el firewall no esté
                ' funcionando.
                ' Para solucionar esto, se lanza un hilo con un tiempo máximo de vida,
                ' si se supera este tiempo, se mata el hilo y se continua con la ejecución
                ' del programa considerando que no se pudieron realizar las operaciones

                Dim Hilo As Thread
                Hilo = New Thread(AddressOf obtenerConfiguradorFirewallInterno)
                Hilo.IsBackground = True
                Hilo.Start()

                Dim FinalizoHilo As Boolean = False
                Dim TiempoEspera As Integer = 0

                While ((FinalizoHilo = False) And (TiempoEspera <= (eTiempoEspera * 10)))
                    FinalizoHilo = Not Hilo.IsAlive
                    If Not FinalizoHilo Then
                        Application.DoEvents()
                        System.Threading.Thread.Sleep(100)
                        TiempoEspera += 1
                    End If
                End While

                Return _OJBETOS_FIREWALL
            End Function
#End Region


#Region " METODOS "
            ''' <summary>
            ''' Obtiene un listado de todas las aplicaciones permitida sen el FireWall de Windows
            ''' </summary>
            ''' <returns>Lista con las aplicaciones permitidas</returns>
            Public Function obtenerProgramasPermitidos(Optional ByVal eTiempoEspera As Short = 5) As List(Of strDatosAplicacion)
                If _OJBETOS_FIREWALL Is Nothing Then Return Nothing        
                Dim paraDevolver As New List(Of strDatosAplicacion)

                Try
                    Dim lasAplicaciones = _OJBETOS_FIREWALL.laPolitica.AuthorizedApplications

                    For Each objApplication In lasAplicaciones
                        Try
                            Dim nuevaAplicacion As New strDatosAplicacion
                            With nuevaAplicacion
                                .NombreAplicacion = objApplication.Name
                                .ReglaActiva = objApplication.Enabled
                                .IPVersion = objApplication.IPVersion
                                .RutaEjecutable = objApplication.ProcessImageFileName
                                .IPRemota = objApplication.RemoteAddresses
                                .Scope = objApplication.Scope
                            End With
                            paraDevolver.Add(nuevaAplicacion)
                        Catch ex As Exception
                            If Log._LOG_ACTIVO Then Log.escribirLog("WARNING Al obtener información de una aplicación permitida en el FireWall.", ex, New StackTrace(0, True))
                        End Try
                    Next
                Catch ex As Exception
                    If Log._LOG_ACTIVO Then Log.escribirLog("ERROR al obtener la lista de aplicaciones permitidas en el Firewall.", ex, New StackTrace(0, True))
                    Return Nothing
                End Try

                Return paraDevolver
            End Function

            ''' <summary>
            ''' Crea una excepción en el FireWall de windows a partir de la ruta del ejecutable
            ''' </summary>
            ''' <param name="eNombreExcepcionFirewall">Nombre que se le dará a la excepción en el FireWall</param>
            ''' <param name="eRutaEjecutable">Ruta del ejecutable donde se creará la excepción</param>
            ''' <returns>True/False dependiendo de si se pudo crear o no la excepción</returns>
            Public Function anhadirExepcionPrograma(ByVal eNombreExcepcionFirewall As String, _
                                                    ByVal eRutaEjecutable As String, _
                                                    Optional ByVal eTiempoEspera As Short = 5) As Boolean
                If _OJBETOS_FIREWALL Is Nothing Then Return False

                ' Se crea el objeto que indica que se va a autorizar una aplicación
                ' y se configura con los parámetros necesarios para permitirla
                Dim laAplicacion As Object = Nothing
                Try
                    laAplicacion = CreateObject("HNetCfg.FwAuthorizedApplication")
                    With laAplicacion
                        .ProcessImageFileName = eRutaEjecutable
                        .Name = eNombreExcepcionFirewall
                        .Scope = NET_FW_SCOPE_ALL
                        .IpVersion = NET_FW_IP_VERSION_ANY
                        .Enabled = True
                    End With
                Catch ex As Exception
                    If Log._LOG_ACTIVO Then Log.escribirLog("ERROR al crear el objeto de creación de excepción en el Firewall de Windows (" & eNombreExcepcionFirewall & ") en " & eRutaEjecutable, ex, New StackTrace(0, True))
                    Return False
                End Try

                ' Se crea la excepción en el Firewall                    
                Try
                    _OJBETOS_FIREWALL.laPolitica.AuthorizedApplications.Add(laAplicacion)
                    If Log._LOG_ACTIVO Then Log.escribirLog("Añadida la excepción al Firewall Windows (" & eNombreExcepcionFirewall & ") en " & eRutaEjecutable, , New StackTrace(0, True))
                Catch ex As Exception
                    If Log._LOG_ACTIVO Then Log.escribirLog("ERROR al añadir excepción al Firewall Windows (" & eNombreExcepcionFirewall & ") en " & eRutaEjecutable, ex, New StackTrace(0, True))
                    Return False
                End Try

                Return True
            End Function

            ''' <summary>
            ''' Elimina una excepcion de un programa añadida al Firewall
            ''' </summary>
            ''' <param name="eRutaEjecutable">Ruta del programa</param>
            ''' <returns>True o False dependiendo de si se pudo eliminar o no</returns>
            Public Function quitarExcepcionPrograma(ByVal eRutaEjecutable As String, _
                                                    Optional ByVal eTiempoEspera As Short = 5) As Boolean
                If _OJBETOS_FIREWALL Is Nothing Then Return False

                Try
                    Dim lasAutorizadas = _OJBETOS_FIREWALL.laPolitica.AuthorizedApplications
                    lasAutorizadas.Remove(eRutaEjecutable)
                Catch ex As Exception
                    If Log._LOG_ACTIVO Then Log.escribirLog("ERROR al quitar excepción al Firewall Windows (" & eRutaEjecutable & ")", ex, New StackTrace(0, True))
                    Return False
                End Try

                Return True
            End Function

            ''' <summary>
            ''' Determina si un programa está añadido como excepción en el FireWall de Windows
            ''' </summary>
            ''' <param name="eRutaEjecutable">Ruta del programa que se quiere comprobar</param>
            ''' <returns>True / False dependiendo de si está permitido o no</returns>
            Public Function existeExcepcionPrograma(ByVal eRutaEjecutable As String, _
                                                    Optional ByVal eTiempoEspera As Short = 5) As Boolean
                Dim paraDevolver As Boolean = False
                Dim losProgramas As List(Of strDatosAplicacion) = obtenerProgramasPermitidos(eTiempoEspera)
                If losProgramas IsNot Nothing Then
                    For Each unPrograma As strDatosAplicacion In losProgramas
                        If unPrograma.RutaEjecutable.ToUpper.Trim = eRutaEjecutable.ToUpper.Trim Then
                            Return True
                        End If
                    Next
                End If

                Return paraDevolver
            End Function

            ''' <summary>
            ''' Obtiene un listado de todas las aplicaciones permitida sen el FireWall de Windows
            ''' </summary>
            ''' <returns>Lista con las aplicaciones permitidas</returns>
            Public Function obtenerPuertosPermitidos(Optional ByVal eTiempoEspera As Short = 5) As List(Of strDatosPuerto)
                If _OJBETOS_FIREWALL Is Nothing Then Return Nothing

                Dim paraDevolver As New List(Of strDatosPuerto)

                Try
                    Dim losPuertos = _OJBETOS_FIREWALL.laPolitica.GloballyOpenPorts

                    For Each objPuerto In losPuertos
                        Try
                            Dim nuevoPuerto As New strDatosPuerto
                            With nuevoPuerto
                                .NombrePuerto = objPuerto.Name
                                .Puerto = objPuerto.Port
                                .ReglaActiva = objPuerto.Enabled
                                .IPVersion = objPuerto.IPVersion
                                .Protocolo = objPuerto.Protocol
                                .Scope = objPuerto.Scope
                                .IPRemota = objPuerto.RemoteAddresses
                                .BuiltIn = objPuerto.Builtin
                            End With
                            paraDevolver.Add(nuevoPuerto)
                        Catch ex As Exception
                            If Log._LOG_ACTIVO Then Log.escribirLog("WARNING Al obtener información de un puerto permitido en el FireWall.", ex, New StackTrace(0, True))
                        End Try
                    Next
                Catch ex As Exception
                    If Log._LOG_ACTIVO Then Log.escribirLog("ERROR al obtener la lista de puertos permitidos en el Firewall.", ex, New StackTrace(0, True))
                    Return Nothing
                End Try

                Return paraDevolver
            End Function

            ''' <summary>
            ''' Añade una excepción al FireWall de windows en un puerto determinado
            ''' </summary>
            ''' <param name="eNombrePuerto">Nombre para la excepción en el Firewall</param>
            ''' <param name="ePuerto">Puerto que se va a permitir</param>
            ''' <returns>True o False dependiendo de si se pudo ejecutar o no</returns>
            Public Function anhadirExepcionPuerto(ByVal eNombrePuerto As String, _
                                                  ByVal ePuerto As Integer, _
                                                  Optional ByVal eTiempoEspera As Short = 5) As Boolean
                If _OJBETOS_FIREWALL Is Nothing Then Return False

                ' Se crea el objeto que indica que se va a autorizar una aplicación
                ' y se configura con los parámetros necesarios para permitirla
                Dim elPuerto As Object = Nothing
                Try
                    elPuerto = CreateObject("HNetCfg.FWOpenPort")
                    With elPuerto
                        .Name = eNombrePuerto
                        .Protocol = NET_FW_IP_PROTOCOL_TCP
                        .Port = ePuerto
                        .Scope = NET_FW_SCOPE_LOCAL_SUBNET
                        .Enabled = True
                    End With
                Catch EX As Exception
                    If Log._LOG_ACTIVO Then Log.escribirLog("ERROR al crear el objeto de creación de excepción en el Firewall de Windows (" & eNombrePuerto & ") en " & ePuerto, EX, New StackTrace(0, True))
                    Return False
                End Try

                Try
                    _OJBETOS_FIREWALL.laPolitica.GloballyOpenPorts.Add(elPuerto)
                    If Log._LOG_ACTIVO Then Log.escribirLog("Añadida excepción al Firewall Windows (" & eNombrePuerto & ") en " & ePuerto, , New StackTrace(0, True))
                Catch ex As Exception
                    If Log._LOG_ACTIVO Then Log.escribirLog("ERROR al añadir excepción al Firewall Windows (" & eNombrePuerto & ") en " & ePuerto, ex, New StackTrace(0, True))
                    Return False
                End Try

                Return True
            End Function

            ''' <summary>
            ''' Elimina una excepción del Firewall de Windows en un puerto determinado
            ''' </summary>
            ''' <param name="ePuerto">Puerto donde s equitará la excepción</param>
            ''' <returns>True / False dependiendo de si se pudo ejecutar o no</returns>
            Public Function quitarExcepcionPuerto(ByVal ePuerto As Integer, _
                                                  Optional ByVal eTiempoEspera As Short = 5) As Boolean
                If _OJBETOS_FIREWALL Is Nothing Then Return False

                Try
                    Dim losPuertos = _OJBETOS_FIREWALL.laPolitica.GloballyOpenPorts
                    losPuertos.Remove(ePuerto, 6)
                Catch ex As Exception
                    If Log._LOG_ACTIVO Then Log.escribirLog("ERROR al quitar excepción al Firewall Windows (" & ePuerto & ")", ex, New StackTrace(0, True))
                    Return False
                End Try

                Return True
            End Function

            ''' <summary>
            ''' Determina si un puerto está añadido como excepción en el FireWall de Windows
            ''' </summary>
            ''' <param name="ePuerto">Ruta del programa que se quiere comprobar</param>
            ''' <returns>True / False dependiendo de si está permitido o no</returns>
            Public Function existeExcepcionPuerto(ByVal ePuerto As Integer, _
                                                  Optional ByVal eTiempoEspera As Short = 5) As Boolean
                Dim paraDevolver As Boolean = False
                Dim losPuertos As List(Of strDatosPuerto) = obtenerPuertosPermitidos(eTiempoEspera)
                If losPuertos IsNot Nothing Then
                    For Each unPuerto As strDatosPuerto In losPuertos
                        If unPuerto.Puerto = ePuerto Then
                            Return True
                        End If
                    Next
                End If

                Return paraDevolver
            End Function


            ''' <summary>
            ''' Permite los paquetes ICMP de tipo 8 (PING) en el firewall
            ''' </summary>
            ''' <returns>True o False dependiendo de si se pudo añadir la excepción </returns>
            Public Function permitirPING(Optional ByVal eTiempoEspera As Short = 5) As Boolean
                If _OJBETOS_FIREWALL Is Nothing Then Return False

                Try
                    _OJBETOS_FIREWALL.laPolitica.IcmpSettings.AllowInboundEchoRequest = True
                    If Log._LOG_ACTIVO Then Log.escribirLog("El FireWall permite PING.", , New StackTrace(0, True))
                Catch ex As Exception
                    If Log._LOG_ACTIVO Then Log.escribirLog("ERROR al añadir excepción PING al Firewall Windows", ex, New StackTrace(0, True))
                    Return False
                End Try
                Return True
            End Function

            ''' <summary>
            ''' Bloquea los paquetes ICMP de tipo 8 (PING) en el firewall
            ''' </summary>
            ''' <returns>True o False dependiendo de si se pudo añadir la excepción </returns>
            Public Function bloquearPING(Optional ByVal eTiempoEspera As Short = 5) As Boolean
                If _OJBETOS_FIREWALL Is Nothing Then Return False

                Try
                    _OJBETOS_FIREWALL.laPolitica.IcmpSettings.AllowInboundEchoRequest = False
                    If Log._LOG_ACTIVO Then Log.escribirLog("El FireWall bloquea PING.", , New StackTrace(0, True))
                Catch ex As Exception
                    If Log._LOG_ACTIVO Then Log.escribirLog("ERROR al bloquear las peticiones PING al Firewall Windows", ex, New StackTrace(0, True))
                    Return False
                End Try

                Return True
            End Function

            ''' <summary>
            ''' Activa el FireWall de Windows
            ''' </summary>
            ''' <returns>True o False dependiendo de si se pudo activar o no</returns>
            Public Function activarFirewall(Optional ByVal eTiempoEspera As Short = 5) As Boolean
                If _OJBETOS_FIREWALL Is Nothing Then Return False

                Try
                    _OJBETOS_FIREWALL.laPolitica.FirewallEnabled = True
                    If Log._LOG_ACTIVO Then Log.escribirLog("El FireWall se activó correctamente.", , New StackTrace(0, True))
                Catch ex As Exception
                    If Log._LOG_ACTIVO Then Log.escribirLog("ERROR al activar el Firewall", ex, New StackTrace(0, True))
                    Return False
                End Try

                Return True
            End Function

            ''' <summary>
            ''' Dessactiva el FireWall de Windows
            ''' </summary>
            ''' <returns>True o False dependiendo de si se pudo desactivar o no</returns>
            Public Function desactivarFirewall(Optional ByVal eTiempoEspera As Short = 5) As Boolean
         If _OJBETOS_FIREWALL Is Nothing Then Return False

                Try
                    _OJBETOS_FIREWALL.laPolitica.FirewallEnabled = False
                    If Log._LOG_ACTIVO Then Log.escribirLog("El FireWall se desactivó correctamente.", , New StackTrace(0, True))
                Catch ex As Exception
                    If Log._LOG_ACTIVO Then Log.escribirLog("ERROR al desactivar el Firewall", ex, New StackTrace(0, True))
                    Return False
                End Try

                Return True
            End Function
#End Region

#Region " IDISPOSABLE IMPLEMENTACION "
            Private disposedValue As Boolean ' Para detectar llamadas redundantes

            ' IDisposable
            Protected Overridable Sub Dispose(disposing As Boolean)
                If Not Me.disposedValue Then
                    If disposing Then
                        ' TODO: desechar estado administrado (objetos administrados).
                    End If

                    ' TODO: liberar recursos no administrados (objetos no administrados) e invalidar Finalize() below.
                    ' TODO: Establecer campos grandes como Null.
                End If
                Me.disposedValue = True
            End Sub

            ' TODO: invalidar Finalize() sólo si la instrucción Dispose(ByVal disposing As Boolean) anterior tiene código para liberar recursos no administrados.
            'Protected Overrides Sub Finalize()
            '    ' No cambie este código. Ponga el código de limpieza en la instrucción Dispose(ByVal disposing As Boolean) anterior.
            '    Dispose(False)
            '    MyBase.Finalize()
            'End Sub

            ' Visual Basic agregó este código para implementar correctamente el patrón descartable.
            Public Sub Dispose() Implements IDisposable.Dispose
                ' No cambie este código. Coloque el código de limpieza en Dispose(disposing As Boolean).
                Dispose(True)
                GC.SuppressFinalize(Me)
            End Sub
#End Region

        End Class
    End Namespace
End Namespace

