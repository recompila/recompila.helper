﻿Imports System.Globalization
Imports System.ComponentModel
Imports System.Threading
Imports System.Windows.Forms
Imports System.Drawing
Imports ComponentFactory.Krypton.Toolkit

Namespace Formularios
    Public Module modFormularios
        Public Enum PosicionesFormulario
            CentradoFormulario
            CentradoPantalla
            EsquinaSuperiorIzquierda
            ControladoPorWindows
        End Enum

        ''' <summary>
        ''' Centra el formulario que se le pasa por referencia en la pantalla
        ''' </summary>
        ''' <param name="elFormulario">Formulario a centrar en la pantalla</param>
        Public Sub centrarVentana(ByRef elFormulario As System.Windows.Forms.Form)
            elFormulario.Top = (My.Computer.Screen.Bounds.Height / 2) - (elFormulario.Height / 2)
            elFormulario.Left = (My.Computer.Screen.Bounds.Width / 2) - (elFormulario.Width / 2)
        End Sub

        ''' <summary>
        ''' Centra los componentes que tienen el TAG con C en el Formulario
        ''' </summary>
        ''' <param name="controlConcreto">Ventana en la que se quierer centrar los formularios</param>
        ''' <remarks></remarks>
        Public Sub centrarComponentes(ByRef controlConcreto As Control)
            For Each C As Control In controlConcreto.Controls
                If TypeOf (C) Is Panel And C.Tag = "C" Then
                    CType(C, Panel).Left = (controlConcreto.Width / 2) - (C.Width / 2)
                End If

                ' Se vuelve a ejecutar de forma recursiva ya que hay controles que tienen
                ' a su vez grupos de controles.
                centrarComponentes(C)
            Next
        End Sub

        ''' <summary>
        ''' Se encarga de limpiar todos los controles de una ventana que cumplan la condición
        ''' especificada en el criterio del TAG
        ''' </summary>
        ''' <param name="controlConcreto">Sobre que control queremos actuar, si es todo el formulario
        ''' se haría sobre me.</param>
        ''' <param name="elTag">Que tiene que aparecer en el TAG para que se limpie el campo.</param>
        ''' <param name="mantenerItemsListas">En el caso de comboBoxes y Listados limpiar los campos..</param>
        Public Sub limpiarCampos(ByVal controlConcreto As Control, _
         Optional ByVal elTag As String = "", _
         Optional ByVal mantenerItemsListas As Boolean = False)

            ' Para evitar el control del TAG si no tiene nada que comparar, para ello
            ' se añade a la condición un OR con un TRUE si el Tag es cierto para 
            ' que el resultado siempre sea TRUE
            Dim saltaTag As Boolean = False
            If elTag = "" Then saltaTag = True

            ' Se recorren todos los controles del formulario limpiando los
            ' campos de los que cumplan los criterios de borrado, en cada
            ' formulario se definen los criterios mediante los TAG de los
            ' controles y un enumerado para identificar cada criterio
            For Each C As Control In controlConcreto.Controls
                If TypeOf (C) Is TextBox And (C.Tag = elTag Or saltaTag) Then
                    CType(C, TextBox).Clear()
                End If

                If TypeOf (C) Is System.Windows.Forms.ListBox And (C.Tag = elTag Or saltaTag) Then
                    CType(C, System.Windows.Forms.ListBox).DataSource = Nothing
                    CType(C, System.Windows.Forms.ListBox).Items.Clear()
                End If

                If TypeOf (C) Is System.Windows.Forms.ComboBox And (C.Tag = elTag Or saltaTag) Then
                    If Not mantenerItemsListas Then
                        CType(C, System.Windows.Forms.ComboBox).DataSource = Nothing
                        CType(C, System.Windows.Forms.ComboBox).Items.Clear()
                    Else
                        If CType(C, System.Windows.Forms.ComboBox).Items.Count > 0 Then CType(C, System.Windows.Forms.ComboBox).SelectedIndex = -1
                    End If

                    CType(C, System.Windows.Forms.ComboBox).Text = ""
                End If

                If TypeOf (C) Is System.Windows.Forms.ListView And (C.Tag = elTag Or saltaTag) Then
                    If Not mantenerItemsListas Then
                        CType(C, System.Windows.Forms.ListView).Items.Clear()
                    End If
                End If

                If TypeOf (C) Is System.Windows.Forms.CheckedListBox And (C.Tag = elTag Or saltaTag) Then
                    If Not mantenerItemsListas Then
                        CType(C, System.Windows.Forms.CheckedListBox).DataSource = Nothing
                        CType(C, System.Windows.Forms.CheckedListBox).Items.Clear()
                    End If
                End If

                If TypeOf (C) Is System.Windows.Forms.MaskedTextBox And (C.Tag = elTag Or saltaTag) Then
                    CType(C, System.Windows.Forms.MaskedTextBox).Clear()
                End If

                If TypeOf (C) Is System.Windows.Forms.CheckBox And (C.Tag = elTag Or saltaTag) Then
                    CType(C, System.Windows.Forms.CheckBox).Checked = False
                End If

                If TypeOf (C) Is System.Windows.Forms.RadioButton And (C.Tag = elTag Or saltaTag) Then
                    CType(C, System.Windows.Forms.RadioButton).Checked = False
                End If

                If TypeOf (C) Is System.Windows.Forms.PictureBox And (C.Tag = elTag Or saltaTag) Then
                    CType(C, System.Windows.Forms.PictureBox).Image = Nothing
                End If

                If TypeOf (C) Is System.Windows.Forms.NumericUpDown And (C.Tag = elTag Or saltaTag) Then
                    CType(C, System.Windows.Forms.NumericUpDown).Value = CType(C, System.Windows.Forms.NumericUpDown).Minimum
                End If

                If TypeOf (C) Is System.Windows.Forms.TreeView And (C.Tag = elTag Or saltaTag) Then
                    CType(C, System.Windows.Forms.TreeView).Nodes.Clear()
                End If

                ' Se vuelve a ejecutar de forma recursiva ya que hay controles que tienen
                ' a su vez grupos de controles.
                limpiarCampos(C, elTag, mantenerItemsListas)
            Next
        End Sub

        ''' <summary>
        ''' Modifica la tecla ENTER para que actúe como la tecla TAB
        ''' </summary>
        Public Sub CambiarIntrosPorTabs(ByRef Formulario As Form)
            'Activamos el keypreview
            Formulario.KeyPreview = True

            'Controlamos el evento KeyPress
            RemoveHandler Formulario.KeyPress, AddressOf CambioEnter
            AddHandler Formulario.KeyPress, AddressOf CambioEnter
        End Sub

        ''' <summary>
        ''' Funcion que realiza el cambio de funcionalidad de la tecla enter para que se comporte como la tecla tab
        ''' </summary>
        Public Sub CambioEnter(ByVal sender As Object, ByVal e As KeyPressEventArgs)
            If e.KeyChar <> ChrW(Keys.Enter) Then Exit Sub

            Dim ControlActivo As Control = DirectCast(sender, Form).ActiveControl

            While TypeOf ControlActivo Is IContainerControl
                ControlActivo = DirectCast(ControlActivo, IContainerControl).ActiveControl
            End While

            If TypeOf ControlActivo Is ComponentFactory.Krypton.Toolkit.KryptonTextBox AndAlso DirectCast(ControlActivo, ComponentFactory.Krypton.Toolkit.KryptonTextBox).Multiline Then
                Exit Sub
            ElseIf TypeOf ControlActivo Is TextBox AndAlso DirectCast(ControlActivo, TextBox).Multiline Then
                Exit Sub
            Else
                e.KeyChar = ChrW(Keys.Tab)
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        End Sub
    End Module

    Public Module modFormulariosMDI
        ''' <summary>
        ''' Pinta el color de fondo de un formulario mdiParent con el establecido en la propiedad BackColor
        ''' </summary>
        ''' <param name="Formulario">Formulario mdiParent que queremos colorear</param>
        Public Sub EstablecerBackColorMDI(ByRef Formulario As Form)
            If Not Formulario.IsMdiContainer Then Exit Sub

            'Recorro los controles buscando la ventana contenedora
            For Each Ctrl As Control In Formulario.Controls
                If TypeOf (Ctrl) Is MdiClient Then
                    'Establezco el color de fondo
                    Ctrl.BackColor = Formulario.BackColor
                End If
            Next
        End Sub

        ''' <summary>
        ''' Comprueba que no existan más formularios abiertos del tipo deseado antes de crear una nueva instancia
        ''' </summary>
        ''' <param name="FormularioContenedor">Formulario mdiParent que se utilizará como contenedor del nuevo formulario</param>
        ''' <param name="TipoFormulario">Tipo de formulario que creemos crear. Llamar con Formulario.GetType.ToString()</param>
        ''' <param name="senderImage">Imagen que se utilizará como icono del formulario</param>
        ''' <param name="PosicionVentana">Indica el lugar por defecto donde se mostrará la ventana</param>
        ''' <param name="NumeroMaximoVentanas">Número máximo de instancias permitidas para ese tipo de formulario</param>
        ''' <remarks></remarks>
        Public Function AbrirFormularioUnico(ByRef FormularioContenedor As Form, ByVal TipoFormulario As String, Optional ByVal senderImage As Image = Nothing, Optional ByVal PosicionVentana As PosicionesFormulario = PosicionesFormulario.EsquinaSuperiorIzquierda, Optional ByVal NumeroMaximoVentanas As Integer = 1) As Form
            Dim FormulariosEncontrados As Int16 = 0

            'Si no es un MDI, no me sirve de nada
            If FormularioContenedor IsNot Nothing AndAlso FormularioContenedor.IsMdiContainer Then
                If NumeroMaximoVentanas <> -1 Then
                    'Recorro los formularios mirando de que tipo son
                    For Each f As Form In FormularioContenedor.MdiChildren
                        If f.GetType().ToString = TipoFormulario Then
                            'Si son del mismo tipo que el mio, miro si llegué al máximo de formulario permitidos
                            FormulariosEncontrados += 1
                            If FormulariosEncontrados = NumeroMaximoVentanas Then
                                f.Activate()
                                Return f
                            End If
                        End If
                    Next
                End If
            End If

            Dim Formulario As Form = ObtenerFormularioPorTipo(TipoFormulario)
            If Formulario IsNot Nothing Then

                If Not IsNothing(senderImage) Then Formulario.Icon = Imagenes.imagenAIcono(senderImage.Clone())
                If FormularioContenedor IsNot Nothing AndAlso FormularioContenedor.IsMdiContainer Then Formulario.MdiParent = FormularioContenedor

                Select Case PosicionVentana
                    Case PosicionesFormulario.CentradoFormulario
                        Formulario.StartPosition = FormStartPosition.CenterParent
                    Case PosicionesFormulario.CentradoPantalla
                        Formulario.StartPosition = FormStartPosition.CenterScreen
                    Case PosicionesFormulario.ControladoPorWindows
                        Formulario.StartPosition = FormStartPosition.WindowsDefaultLocation
                    Case PosicionesFormulario.EsquinaSuperiorIzquierda
                        Formulario.Location = New Point(0, 0)
                        Formulario.StartPosition = FormStartPosition.Manual
                End Select
            End If

            Return Formulario
        End Function

        ''' <summary>
        ''' Obtiene un formulario por su tipo
        ''' </summary>
        ''' <param name="TipoFormulario">Tipo de formulario que creemos crear. Llamar con Formulario.GetType.ToString()</param>
        ''' <remarks></remarks>
        Public Function ObtenerFormularioPorTipo(ByVal TipoFormulario As String, ByVal ParamArray Argumentos() As Object) As Form
            Try
                ' Creo la ventana
                Dim Formulario As Form = CType(Activator.CreateInstance(Type.GetType(TipoFormulario), Argumentos), Form)
                Return Formulario
            Catch ex As System.Exception
#If DEBUG Then
                Debugger.Break()
#Else
                    MessageBox.Show("Error al abrir la ventana: " & ex.Message)
#End If
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' Cambia la configuración de idioma de la aplicación
        ''' </summary>
        ''' <param name="Idioma">Idioma de la interfaz gráfica</param>
        Public Sub CambiarIdiomaAplicacion(ByVal Idioma As String)
            ' Make a CultureInfo and ComponentResourceManager.
            Dim culture_info As New CultureInfo(Idioma)

            Thread.CurrentThread.CurrentUICulture = culture_info
            Thread.CurrentThread.CurrentCulture = culture_info

        End Sub

        Public Sub maximizarEnPadre(ByRef ePadre As Form, _
                                    ByVal eFormulario As Form)

            If eFormulario IsNot Nothing AndAlso Nothing Then
                Dim AltoResta As Long = Math.Abs(eFormulario.Size.Height - eFormulario.PreferredSize.Height) + 5
                Dim AnchoResta As Long = Math.Abs(eFormulario.Size.Width - eFormulario.PreferredSize.Width) + 5

                Dim NuevoTamanho As New Point(eFormulario.Size.Width - AnchoResta, eFormulario.Size.Height - AltoResta)

                If NuevoTamanho.X > eFormulario.MinimumSize.Width Then eFormulario.Width = NuevoTamanho.X
                If NuevoTamanho.Y > eFormulario.MinimumSize.Height Then eFormulario.Height = NuevoTamanho.Y
            End If

            eFormulario.Location = New Point(0, 0)
        End Sub
    End Module

    Namespace Funcionalidad
        Public Module modQuadraliaFormulariosFuncionalidad
#Region " COMPORTAMIENTO "
            Public Sub ManejadorTabPorIntro(ByRef UnControl As Control)
                If TypeOf (UnControl) Is KryptonTextBox Then
                    If CType(UnControl, KryptonTextBox).Multiline = False Then
                        AddHandler CType(UnControl, KryptonTextBox).KeyDown, AddressOf TabPorIntro
                    End If
                ElseIf TypeOf (UnControl) Is KryptonComboBox Or TypeOf (UnControl) Is KryptonCheckBox Or TypeOf (UnControl) Is KryptonRadioButton Or TypeOf (UnControl) Is KryptonDateTimePicker Or TypeOf (UnControl) Is KryptonNumericUpDown Then
                    AddHandler UnControl.KeyDown, AddressOf TabPorIntro
                End If
            End Sub

            Public Sub TabPorIntro(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
                If sender IsNot Nothing Then
                    Dim SeAplica As Boolean = True
                    If TypeOf (sender) Is KryptonTextBox AndAlso CType(sender, KryptonTextBox).Multiline = True Then SeAplica = False
                    If SeAplica Then
                        If e.KeyCode = Keys.Return Then
                            e.Handled = True
                            e.SuppressKeyPress = True
                            SendKeys.Send("{Tab}")
                        End If
                    End If
                End If
            End Sub
#End Region
        End Module
    End Namespace
End Namespace