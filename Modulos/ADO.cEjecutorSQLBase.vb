﻿Namespace ADO
    ''' <summary>
    ''' Clase base de la que van a heredar todos los ejecutores SQL que se 
    ''' pueden implementar. Junto con la Interfaz IEjecutorSQL permiten la
    ''' descenctralización de todos los objetos
    ''' </summary>
    Public Class cEjecutorSQLBase
#Region " PROPIEDADES"
        ''' <summary>
        ''' Datos de la conexión con la base de datos
        ''' </summary>
        Public Property DatosConexion As ADO.cConfiguracionAcceso
            Get
                Return iDatosConexion
            End Get
            Set(ByVal value As ADO.cConfiguracionAcceso)
                iDatosConexion = value
            End Set
        End Property
        Private iDatosConexion As ADO.cConfiguracionAcceso = Nothing
#End Region

#Region " CONSTRUCTORES"
        ''' <summary>
        ''' Crea una nueva instancia de la clase para poder trabajar con la base de datos que se le pasa como parametro
        ''' </summary>
        Public Sub New(ByVal eDatosConexion As ADO.cConfiguracionAcceso)
            iDatosConexion = eDatosConexion
        End Sub
#End Region

#Region " METODOS COMPARTIDOS "
        ''' <summary>
        ''' Comprueba si una consulta SQL tiene el WHERE, si no lo tiene se lo añade, en caso de 
        ''' existir le añade un AND
        ''' </summary>
        ''' <param name="eConsulta">Consulta sobre la que se va a trabajar.</param>
        ''' <param name="eCondicion">Condición que se va a añadir</param>
        ''' <param name="eNexo">Nexto AND o OR para unir las condiciones</param>
        ''' <remarks>Solamente se debe utilizar con consultas simples, si la consulta
        ''' tiene una subconsulta donde existe un WHERE fallará</remarks>
        Public Shared Sub anhadirCondicion(ByRef eConsulta As String, _
                                           Optional ByVal eCondicion As String = "", _
                                           Optional ByVal eNexo As String = "AND")
            Dim PosicionFor As Long = eConsulta.LastIndexOf("FROM")
            Dim TextoComparar As String = eConsulta.Substring(PosicionFor)

            If InStr(TextoComparar, "WHERE") > 0 Then
                eConsulta &= " " & eNexo & " "
            Else
                eConsulta &= " WHERE "
            End If

            If Not String.IsNullOrEmpty(eCondicion) Then eConsulta &= eCondicion & " "
        End Sub

        ''' <summary>
        ''' Escribe la consulta que se le pasa por parametro para poder analizarla o transportarla
        ''' a otros programas. Para ello se crea un fichero temporal y se muestra en el notepad.
        ''' </summary>
        ''' <param name="eConsulta">Consulta a guardar y mostrar.</param>
        ''' <remarks></remarks>
        Public Shared Sub escribirConsulta(ByVal eConsulta As String)
            Dim elArchivoTemporal As String = String.Empty
            Dim elEscritor As System.IO.StreamWriter = Nothing

            Try
                elArchivoTemporal = Ficheros.obtenerFicheroTemporal & ".txt"

                elEscritor = New System.IO.StreamWriter(elArchivoTemporal, False)
                elEscritor.Write(eConsulta)

                Shell("notepad " & "" & elArchivoTemporal & "", AppWinStyle.NormalFocus)
            Catch ex As Exception
                If Log._LOG_ACTIVO Then Log.escribirLog("Error al guardar la consulta '" & eConsulta & "' en un archivo temporal...", ex, New StackTrace(0, True))
            Finally
                If elEscritor IsNot Nothing Then
                    elEscritor.Close()
                    elEscritor.Dispose()
                End If
            End Try
        End Sub
#End Region
    End Class
End Namespace

